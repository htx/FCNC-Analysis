#!/bin/python
import os
import time, getpass
import sys
import datetime
from ROOT import *

sys.path.append( os.getenv("ROOTCOREBIN") + "/python/FCNC-Analysis/")
from Analysis_Samples import *

sys.path.append( os.getenv("ROOTCOREBIN") + "/python/IFAETopFramework/" )
from BatchTools import *
from Samples import *

##------------------------------------------------------
## Checking the arguments
##------------------------------------------------------
if(len(sys.argv)<3):
    printWarning(sys.argv[0] + " ==> Wrong input arguments")
    print ""
    print "    python "+sys.argv[0]+" [arg]"
    print ""
    print "Arguments"
    print "========="
    print "    inputDir=<path to input files>"
    print "    useData=<TRUE/FALSE> use the real data"
    print "    [outputDir]=<place where to store the output files>"
    print ""
    sys.exit()

##------------------------------------------------------
## Defines some useful variables
##------------------------------------------------------
now = datetime.datetime.now().strftime("%Y_%m_%d_%H%M")
here = os.getcwd()

##------------------------------------------------------
## Selects the arguments
##------------------------------------------------------
inputDir=""
useData = True
folderRootFiles = "RootFilesForTtHFitter_"+now
useSystematics = True
doAllBR=False
for iArg in range(1,len(sys.argv)):
    splitted=sys.argv[iArg].split("=")
    argument = splitted[0].upper()
    if(argument=="INPUTDIR"): inputDir = splitted[1]
    elif(argument=="USEDATA"):
        if splitted[1].upper()=="TRUE":
            useData = True
        elif splitted[1].upper()=="FALSE":
            useData = False
        else:
            printWarning("/!\ The argument for useData is not recognised ... Please check !")
    elif(argument=="OUTPUTDIR"):
        folderRootFiles = splitted[1]
    elif(argument=="USESYSTEMATICS"):
        if splitted[1].upper()=="TRUE":
            useSystematics = True
        elif splitted[1].upper()=="FALSE":
            useSystematics = False
        else:
            printWarning("/!\ The argument for useSystematics is not recognised ... Please check !")
    elif(argument=="ALLBR"):
        if splitted[1].upper()=="TRUE":
            doAllBR = True
        elif splitted[1].upper()=="FALSE":
            doAllBR = False
        else:
            printWarning("/!\ The argument for allBR is not recognised ... Please check !")
    else:
        printWarning("/!\ Unrecognized argument ("+splitted[0]+") ! Please check !")
if(inputDir==""):
    printError("<!> Please provide an input config file to use !")
    sys.exit()

##------------------------------------------------------
## Creating the output repository
##------------------------------------------------------
os.system("mkdir -p "+folderRootFiles)

##------------------------------------------------------
## Getting all samples and their associated weight/object systematics
##------------------------------------------------------
Samples = []
# -- Data
if(useData):
    Samples += GetDataSamples( data_type = "TOPQ1" )
#-- MC backgrounds and 4tops signals
Samples += GetTtbarSamples( useObjectSyst=useSystematics, hfSplitted = True, ttbarSystSamples = False )
Samples += GetOtherSamples( useObjectSyst=useSystematics )
printGoodNews("--> All samples recovered")
##........................................................

##------------------------------------------------------
## Loop over the samples and systematics
##------------------------------------------------------
Combination = []
Commands = []
os.chdir(inputDir)
listfiles = glob.glob("outFCNCAnalysis_*.root")
for sample in Samples:
    
    SName = sample['name'] # sample name
    SType = sample['sampleType']

    print "-> Sample: " + SType + " DSID " + SName

    cleaned_sampleType = SType.replace("#","").replace(" ","").replace("{","").replace("}","").replace("+","").replace("(","").replace(")","")

    #Do the list of systematics
    systList = []
    for Systematic in sample['objSyst']:
        if Systematic['oneSided']:
            systList += [Systematic['name']]
        else:
            systList += [Systematic['nameUp']]
            systList += [Systematic['nameDown']]

    ##------------------------------------------------------
    ## Loop over the systematics
    ##------------------------------------------------------
    for syst in systList:
        #print "   -> Syst: " + syst
        name_temp_rootfile = folderRootFiles+"/"+cleaned_sampleType

        if syst.upper().find("NOMINAL")==-1:
            name_temp_rootfile += "_"+syst
        name_temp_rootfile += ".root"

        if not name_temp_rootfile in Combination:
            Combination += [name_temp_rootfile]
            com = "hadd " + name_temp_rootfile
            usedFiles = []
            for f in listfiles:
                if f.find("outFCNCAnalysis_"+SType+"_")>-1 and f.find(syst)>-1:
                    com += " " + f
                    usedFiles += [f]
            Commands += [com]
            for usedfile in usedFiles:
                listfiles.remove(usedfile)
            usedFiles = []
        else:
            counter = 0
            for Comb in Combination:
                if(Comb==name_temp_rootfile):
                    usedFiles = []
                    for f in listfiles:
                        if f.find("outFCNCAnalysis_"+SType+"_")>-1 and f.find(syst)>-1:
                            Commands[counter] += " " + f
                            usedFiles += [f]
                    for usedfile in usedFiles:
                        listfiles.remove(usedfile)
                    usedFiles = []
                counter += 1

##------------------------------------------------------
## Actually adding the rootfiles
##------------------------------------------------------
for Comm in Commands:
    command = Comm
    splittedCommand = Comm.split(" ")
    if len(splittedCommand)==3:
        com = "cp "+splittedCommand[2]+" "+splittedCommand[1]
        printGoodNews("-> Copying file for sample: " + splittedCommand[1])
        os.system(com)
    elif len(splittedCommand)>3:
        printGoodNews("-> Hadding files for sample: " + splittedCommand[1])
        print(Comm)
        os.system(Comm)
    else:
        printError( "Invalid command line: " + Comm )

# After running all hadd commands get back to the initial folder 
os.chdir(here)
