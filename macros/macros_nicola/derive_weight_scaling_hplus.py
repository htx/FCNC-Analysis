import os
import sys

# Run from python folder 

input_folder='/nfs/at3/scratch2/aivina/Fit_Inputs_2020/'
list_of_files=['ttbarbb.root','ttbarcc.root','ttbarlight.root']

list_of_weight_names=['nomWeight_weight_mc','o_nom_weight_index_1','o_nom_weight_index_2','o_nom_weight_index_3','o_nom_weight_index_4','o_nom_weight_index_193','o_nom_weight_index_194','o_nom_weight_index_198','o_nom_weight_index_199']

for input_file in list_of_files: 
    for weight in list_of_weight_names: 
        print('processing file '+input_file+' and weight '+weight)
        command='python addup_weighted_events.py '+input_folder+'/'+input_file+' -t tree -l '+weight
        print(command)
        os.system(command)
        
