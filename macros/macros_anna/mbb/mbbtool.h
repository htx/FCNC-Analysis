#ifndef mbbtool_h
#define mbbtool_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TMath.h>
#include <TTree.h>
#include <TString.h>
#include <TH1.h>
#include "prun_helper.h"

#include <vector>
#include <iostream>
#include <fstream>


class mbbtool {
public :
   TTree           *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   TTree *jj;
   TH1D  *m_bb_histo;
// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_oldTriggerSF;
   Float_t         weight_bTagSF_MV2c10_60;
   Float_t         weight_bTagSF_MV2c10_70;
   Float_t         weight_bTagSF_MV2c10_77;
   Float_t         weight_bTagSF_MV2c10_85;
   Float_t         weight_bTagSF_MV2c10_Continuous;
   Float_t         weight_bTagSF_DL1_60;
   Float_t         weight_bTagSF_DL1_70;
   Float_t         weight_bTagSF_DL1_77;
   Float_t         weight_bTagSF_DL1_85;
   Float_t         weight_bTagSF_DL1_Continuous;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_oldTriggerSF_EL_Trigger_UP;
   Float_t         weight_oldTriggerSF_EL_Trigger_DOWN;
   Float_t         weight_oldTriggerSF_MU_Trigger_STAT_UP;
   Float_t         weight_oldTriggerSF_MU_Trigger_STAT_DOWN;
   Float_t         weight_oldTriggerSF_MU_Trigger_SYST_UP;
   Float_t         weight_oldTriggerSF_MU_Trigger_SYST_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_isTight;
   vector<char>    *el_CF;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<int>     *el_true_type;
   vector<int>     *el_true_origin;
   vector<int>     *el_true_firstEgMotherTruthType;
   vector<int>     *el_true_firstEgMotherTruthOrigin;
   vector<int>     *el_true_firstEgMotherPdgId;
   vector<char>    *el_true_isPrompt;
   vector<char>    *el_true_isChargeFl;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<char>    *mu_isTight;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<int>     *mu_true_type;
   vector<int>     *mu_true_origin;
   vector<char>    *mu_true_isPrompt;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_jvt;
   vector<char>    *jet_passfjvt;
   vector<int>     *jet_truthflav;
   vector<int>     *jet_truthPartonLabel;
   vector<char>    *jet_isTrueHS;
   vector<int>     *jet_truthflavExtended;
   vector<char>    *jet_isbtagged_MV2c10_60;
   vector<char>    *jet_isbtagged_MV2c10_70;
   vector<char>    *jet_isbtagged_MV2c10_77;
   vector<char>    *jet_isbtagged_MV2c10_85;
   vector<int>     *jet_tagWeightBin_MV2c10_Continuous;
   vector<char>    *jet_isbtagged_DL1_60;
   vector<char>    *jet_isbtagged_DL1_70;
   vector<char>    *jet_isbtagged_DL1_77;
   vector<char>    *jet_isbtagged_DL1_85;
   vector<int>     *jet_tagWeightBin_DL1_Continuous;
   vector<float>   *jet_DL1;
   vector<float>   *ljet_pt;
   vector<float>   *ljet_eta;
   vector<float>   *ljet_phi;
   vector<float>   *ljet_e;
   vector<float>   *ljet_m;
   vector<float>   *ljet_sd12;
   Float_t         met_met;
   Float_t         met_phi;
   Char_t          HLT_mu50;
   Char_t          HLT_e60_lhmedium_nod0;
   Char_t          HLT_e140_lhloose_nod0;
   Char_t          HLT_e26_lhtight_nod0_ivarloose;
   Char_t          HLT_mu26_ivarmedium;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium_nod0;
   vector<char>    *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
   vector<char>    *el_trigMatch_HLT_e140_lhloose_nod0;
   vector<char>    *mu_trigMatch_HLT_mu50;
   vector<char>    *mu_trigMatch_HLT_mu26_ivarmedium;
   vector<int>     *jet_tagWeightBin;
   vector<char>    *el_LHMedium;
   vector<char>    *el_LHTight;
   vector<char>    *el_isPrompt;
   vector<char>    *el_isoFCLoose;
   vector<char>    *el_isoFCTight;
   vector<char>    *el_isoGradient;
   vector<int>     *el_true_pdg;
   vector<float>   *el_true_eta;
   vector<float>   *el_true_pt;
   vector<char>    *mu_Medium;
   vector<char>    *mu_Tight;
   vector<char>    *mu_isPrompt;
   vector<char>    *mu_isoFCLoose;
   vector<char>    *mu_isoFCTight;
   vector<char>    *mu_isoFCTightTrackOnly;
   vector<char>    *mu_isoGradient;
   vector<int>     *mu_true_pdg;
   vector<float>   *mu_true_eta;
   vector<float>   *mu_true_pt;
   Int_t           HF_Classification;
   Int_t           HF_ClassificationGhost;
   Int_t           HF_SimpleClassification;
   Int_t           HF_SimpleClassificationGhost;
   Int_t           HadTop200;
   Int_t           TTbar150;
   Int_t           TopHeavyFlavorFilterFlag;
   Int_t           leptonType;
   Int_t           nBTags_60;
   Int_t           nBTags_70;
   Int_t           nBTags_77;
   Int_t           nBTags_85;
   Int_t           nBTags_DL1_60;
   Int_t           nBTags_DL1_70;
   Int_t           nBTags_DL1_77;
   Int_t           nBTags_DL1_85;
   Int_t           nBTags_MV2c10_60;
   Int_t           nBTags_MV2c10_70;
   Int_t           nBTags_MV2c10_77;
   Int_t           nBTags_MV2c10_85;
   Int_t           nElectrons;
   Int_t           nHFJets;
   Int_t           nJets;
   Int_t           nMuons;
   Int_t           nPDFFlavor;
   Int_t           nPrimaryVtx;
   Int_t           nTaus;
   Int_t           truth_HDecay;
   Int_t           truth_nHiggs;
   Int_t           truth_nLepTop;
   Int_t           truth_nTop;
   Int_t           truth_nVectorBoson;
   Int_t           truth_top_dilep_filter;
   Int_t           ttbb_categories;
   Float_t         HFClassification_q1_eta;
   Float_t         HFClassification_q1_m;
   Float_t         HFClassification_q1_phi;
   Float_t         HFClassification_q1_pt;
   Float_t         HFClassification_q2_eta;
   Float_t         HFClassification_q2_m;
   Float_t         HFClassification_q2_phi;
   Float_t         HFClassification_q2_pt;
   Float_t         HFClassification_qq_dr;
   Float_t         HFClassification_qq_ht;
   Float_t         HFClassification_qq_m;
   Float_t         HFClassification_qq_pt;
   Float_t         truth_hadtop_pt;
   Float_t         truth_higgs_eta;
   Float_t         truth_higgs_pt;
   Float_t         truth_tbar_pt;
   Float_t         truth_top_pt;
   Float_t         truth_ttbar_pt;
   Float_t         ttbb4F_xs_weight;
   Float_t         ttbb_MPIfactor_weight;
   vector<float>   *truth_hadron_pt;
   vector<float>   *truth_hadron_eta;
   vector<float>   *truth_hadron_phi;
   vector<float>   *truth_hadron_m;
   vector<int>     *truth_hadron_pdgid;
   vector<int>     *truth_hadron_status;
   vector<int>     *truth_hadron_barcode;
   vector<int>     *truth_hadron_TopHadronOriginFlag;
   vector<char>    *truth_hadron_type;
   vector<char>    *truth_hadron_final;
   vector<char>    *truth_hadron_initial;
   vector<float>   *truth_pt;
   vector<float>   *truth_eta;
   vector<float>   *truth_phi;
   vector<float>   *truth_m;
   vector<int>     *truth_pdgid;
   vector<int>     *truth_status;
   vector<int>     *truth_barcode;
   vector<Long64_t> *truth_tthbb_info;
   vector<float>   *truth_jet_pt;
   vector<float>   *truth_jet_eta;
   vector<float>   *truth_jet_phi;
   vector<float>   *truth_jet_m;
   vector<int>     *truth_jet_flav;
   vector<int>     *truth_jet_id;
   vector<int>     *truth_jet_count;

   // List of branches
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_oldTriggerSF;   //!
   TBranch        *b_weight_bTagSF_MV2c10_60;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85;   //!
   TBranch        *b_weight_bTagSF_MV2c10_Continuous;   //!
   TBranch        *b_weight_bTagSF_DL1_60;   //!
   TBranch        *b_weight_bTagSF_DL1_70;   //!
   TBranch        *b_weight_bTagSF_DL1_77;   //!
   TBranch        *b_weight_bTagSF_DL1_85;   //!
   TBranch        *b_weight_bTagSF_DL1_Continuous;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_oldTriggerSF_EL_Trigger_UP;   //!
   TBranch        *b_weight_oldTriggerSF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_isTight;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_firstEgMotherTruthType;   //!
   TBranch        *b_el_true_firstEgMotherTruthOrigin;   //!
   TBranch        *b_el_true_firstEgMotherPdgId;   //!
   TBranch        *b_el_true_isPrompt;   //!
   TBranch        *b_el_true_isChargeFl;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_isTight;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_mu_true_isPrompt;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_passfjvt;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_jet_isTrueHS;   //!
   TBranch        *b_jet_truthflavExtended;   //!
   TBranch        *b_jet_isbtagged_MV2c10_60;   //!
   TBranch        *b_jet_isbtagged_MV2c10_70;   //!
   TBranch        *b_jet_isbtagged_MV2c10_77;   //!
   TBranch        *b_jet_isbtagged_MV2c10_85;   //!
   TBranch        *b_jet_tagWeightBin_MV2c10_Continuous;   //!
   TBranch        *b_jet_isbtagged_DL1_60;   //!
   TBranch        *b_jet_isbtagged_DL1_70;   //!
   TBranch        *b_jet_isbtagged_DL1_77;   //!
   TBranch        *b_jet_isbtagged_DL1_85;   //!
   TBranch        *b_jet_tagWeightBin_DL1_Continuous;   //!
   TBranch        *b_ljet_pt;   //!
   TBranch        *b_ljet_eta;   //!
   TBranch        *b_ljet_phi;   //!
   TBranch        *b_ljet_e;   //!
   TBranch        *b_ljet_m;   //!
   TBranch        *b_ljet_sd12;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_el_trigMatch_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_mu_trigMatch_HLT_mu50;   //!
   TBranch        *b_mu_trigMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_jet_tagWeightBin;   //!
   TBranch        *b_el_LHMedium;   //!
   TBranch        *b_el_LHTight;   //!
   TBranch        *b_el_isPrompt;   //!
   TBranch        *b_el_isoFCLoose;   //!
   TBranch        *b_el_isoFCTight;   //!
   TBranch        *b_el_isoGradient;   //!
   TBranch        *b_el_true_pdg;   //!
   TBranch        *b_el_true_eta;   //!
   TBranch        *b_el_true_pt;   //!
   TBranch        *b_mu_Medium;   //!
   TBranch        *b_mu_Tight;   //!
   TBranch        *b_mu_isPrompt;   //!
   TBranch        *b_mu_isoFCLoose;   //!
   TBranch        *b_mu_isoFCTight;   //!
   TBranch        *b_mu_isoFCTightTrackOnly;   //!
   TBranch        *b_mu_isoGradient;   //!
   TBranch        *b_mu_true_pdg;   //!
   TBranch        *b_mu_true_eta;   //!
   TBranch        *b_mu_true_pt;   //!
   TBranch        *b_HF_Classification;   //!
   TBranch        *b_HF_ClassificationGhost;   //!
   TBranch        *b_HF_SimpleClassification;   //!
   TBranch        *b_HF_SimpleClassificationGhost;   //!
   TBranch        *b_HadTop200;   //!
   TBranch        *b_TTbar150;   //!
   TBranch        *b_TopHeavyFlavorFilterFlag;   //!
   TBranch        *b_leptonType;   //!
   TBranch        *b_nBTags_60;   //!
   TBranch        *b_nBTags_70;   //!
   TBranch        *b_nBTags_77;   //!
   TBranch        *b_nBTags_85;   //!
   TBranch        *b_nBTags_DL1_60;   //!
   TBranch        *b_nBTags_DL1_70;   //!
   TBranch        *b_nBTags_DL1_77;   //!
   TBranch        *b_nBTags_DL1_85;   //!
   TBranch        *b_nBTags_MV2c10_60;   //!
   TBranch        *b_nBTags_MV2c10_70;   //!
   TBranch        *b_nBTags_MV2c10_77;   //!
   TBranch        *b_nBTags_MV2c10_85;   //!
   TBranch        *b_nElectrons;   //!
   TBranch        *b_nHFJets;   //!
   TBranch        *b_nJets;   //!
   TBranch        *b_nMuons;   //!
   TBranch        *b_nPDFFlavor;   //!
   TBranch        *b_nPrimaryVtx;   //!
   TBranch        *b_nTaus;   //!
   TBranch        *b_truth_HDecay;   //!
   TBranch        *b_truth_nHiggs;   //!
   TBranch        *b_truth_nLepTop;   //!
   TBranch        *b_truth_nTop;   //!
   TBranch        *b_truth_nVectorBoson;   //!
   TBranch        *b_truth_top_dilep_filter;   //!
   TBranch        *b_ttbb_categories;   //!
   TBranch        *b_HFClassification_q1_eta;   //!
   TBranch        *b_HFClassification_q1_m;   //!
   TBranch        *b_HFClassification_q1_phi;   //!
   TBranch        *b_HFClassification_q1_pt;   //!
   TBranch        *b_HFClassification_q2_eta;   //!
   TBranch        *b_HFClassification_q2_m;   //!
   TBranch        *b_HFClassification_q2_phi;   //!
   TBranch        *b_HFClassification_q2_pt;   //!
   TBranch        *b_HFClassification_qq_dr;   //!
   TBranch        *b_HFClassification_qq_ht;   //!
   TBranch        *b_HFClassification_qq_m;   //!
   TBranch        *b_HFClassification_qq_pt;   //!
   TBranch        *b_truth_hadtop_pt;   //!
   TBranch        *b_truth_higgs_eta;   //!
   TBranch        *b_truth_higgs_pt;   //!
   TBranch        *b_truth_tbar_pt;   //!
   TBranch        *b_truth_top_pt;   //!
   TBranch        *b_truth_ttbar_pt;   //!
   TBranch        *b_truth_hadron_pt;   //!
   TBranch        *b_truth_hadron_eta;   //!
   TBranch        *b_truth_hadron_phi;   //!
   TBranch        *b_truth_hadron_m;   //!
   TBranch        *b_truth_hadron_pdgid;   //!
   TBranch        *b_truth_hadron_status;   //!
   TBranch        *b_truth_hadron_barcode;   //!
   TBranch        *b_truth_hadron_TopHadronOriginFlag;   //!
   TBranch        *b_truth_hadron_type;   //!
   TBranch        *b_truth_hadron_final;   //!
   TBranch        *b_truth_hadron_initial;   //!
   TBranch        *b_truth_pt;   //!
   TBranch        *b_truth_eta;   //!
   TBranch        *b_truth_phi;   //!
   TBranch        *b_truth_m;   //!
   TBranch        *b_truth_pdgid;   //!
   TBranch        *b_truth_status;   //!
   TBranch        *b_truth_barcode;   //!
   TBranch        *b_truth_tthbb_info;   //!
   TBranch        *b_truth_jet_pt;   //!
   TBranch        *b_truth_jet_eta;   //!
   TBranch        *b_truth_jet_phi;   //!
   TBranch        *b_truth_jet_m;   //!
   TBranch        *b_truth_jet_flav;   //!
   TBranch        *b_truth_jet_id;   //!
   TBranch        *b_truth_jet_count;   //!



   mbbtool() {};



   mbbtool(TString);
   virtual ~mbbtool();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   double dr(double phi1, double eta1, double phi2, double eta2);
};


#endif

#ifdef mbbtool_cxx
mbbtool::mbbtool(TString input_files)
{
    TChain *chain = new TChain("nominal_Loose","");
    openFromFileList(input_files, chain);
    Init(chain);

}


mbbtool::~mbbtool()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}


Int_t mbbtool::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}



Long64_t mbbtool::LoadTree(Long64_t entry)
{
   // Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}


void mbbtool::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mc_generator_weights = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_ptvarcone20 = 0;
   el_isTight = 0;
   el_CF = 0;
   el_d0sig = 0;
   el_delta_z0_sintheta = 0;
   el_true_type = 0;
   el_true_origin = 0;
   el_true_firstEgMotherTruthType = 0;
   el_true_firstEgMotherTruthOrigin = 0;
   el_true_firstEgMotherPdgId = 0;
   el_true_isPrompt = 0;
   el_true_isChargeFl = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_ptvarcone30 = 0;
   mu_isTight = 0;
   mu_d0sig = 0;
   mu_delta_z0_sintheta = 0;
   mu_true_type = 0;
   mu_true_origin = 0;
   mu_true_isPrompt = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_mv2c10 = 0;
   jet_jvt = 0;
   jet_passfjvt = 0;
   jet_truthflav = 0;
   jet_truthPartonLabel = 0;
   jet_isTrueHS = 0;
   jet_truthflavExtended = 0;
   jet_isbtagged_MV2c10_60 = 0;
   jet_isbtagged_MV2c10_70 = 0;
   jet_isbtagged_MV2c10_77 = 0;
   jet_isbtagged_MV2c10_85 = 0;
   jet_tagWeightBin_MV2c10_Continuous = 0;
   jet_isbtagged_DL1_60 = 0;
   jet_isbtagged_DL1_70 = 0;
   jet_isbtagged_DL1_77 = 0;
   jet_isbtagged_DL1_85 = 0;
   jet_tagWeightBin_DL1_Continuous = 0;
   ljet_pt = 0;
   ljet_eta = 0;
   ljet_phi = 0;
   ljet_e = 0;
   ljet_m = 0;
   ljet_sd12 = 0;

   el_trigMatch_HLT_e60_lhmedium_nod0 = 0;
   el_trigMatch_HLT_e26_lhtight_nod0_ivarloose = 0;
   el_trigMatch_HLT_e140_lhloose_nod0 = 0;
   mu_trigMatch_HLT_mu50 = 0;
   mu_trigMatch_HLT_mu26_ivarmedium = 0;
   jet_tagWeightBin = 0;
   el_LHMedium = 0;
   el_LHTight = 0;
   el_isPrompt = 0;
   el_isoFCLoose = 0;
   el_isoFCTight = 0;
   el_isoGradient = 0;
   el_true_pdg = 0;
   el_true_eta = 0;
   el_true_pt = 0;
   mu_Medium = 0;
   mu_Tight = 0;
   mu_isPrompt = 0;
   mu_isoFCLoose = 0;
   mu_isoFCTight = 0;
   mu_isoFCTightTrackOnly = 0;
   mu_isoGradient = 0;
   mu_true_pdg = 0;
   mu_true_eta = 0;
   mu_true_pt = 0;
   truth_hadron_pt = 0;
   truth_hadron_eta = 0;
   truth_hadron_phi = 0;
   truth_hadron_m = 0;
   truth_hadron_pdgid = 0;
   truth_hadron_status = 0;
   truth_hadron_barcode = 0;
   truth_hadron_TopHadronOriginFlag = 0;
   truth_hadron_type = 0;
   truth_hadron_final = 0;
   truth_hadron_initial = 0;
   truth_pt = 0;
   truth_eta = 0;
   truth_phi = 0;
   truth_m = 0;
   truth_pdgid = 0;
   truth_status = 0;
   truth_barcode = 0;
   truth_tthbb_info = 0;
   truth_jet_pt = 0;
   truth_jet_eta = 0;
   truth_jet_phi = 0;
   truth_jet_m = 0;
   truth_jet_flav = 0;
   truth_jet_id = 0;
   truth_jet_count = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_oldTriggerSF", &weight_oldTriggerSF, &b_weight_oldTriggerSF);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_60", &weight_bTagSF_MV2c10_60, &b_weight_bTagSF_MV2c10_60);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70", &weight_bTagSF_MV2c10_70, &b_weight_bTagSF_MV2c10_70);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77", &weight_bTagSF_MV2c10_77, &b_weight_bTagSF_MV2c10_77);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85", &weight_bTagSF_MV2c10_85, &b_weight_bTagSF_MV2c10_85);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_Continuous", &weight_bTagSF_MV2c10_Continuous, &b_weight_bTagSF_MV2c10_Continuous);
   fChain->SetBranchAddress("weight_bTagSF_DL1_60", &weight_bTagSF_DL1_60, &b_weight_bTagSF_DL1_60);
   fChain->SetBranchAddress("weight_bTagSF_DL1_70", &weight_bTagSF_DL1_70, &b_weight_bTagSF_DL1_70);
   fChain->SetBranchAddress("weight_bTagSF_DL1_77", &weight_bTagSF_DL1_77, &b_weight_bTagSF_DL1_77);
   fChain->SetBranchAddress("weight_bTagSF_DL1_85", &weight_bTagSF_DL1_85, &b_weight_bTagSF_DL1_85);
   fChain->SetBranchAddress("weight_bTagSF_DL1_Continuous", &weight_bTagSF_DL1_Continuous, &b_weight_bTagSF_DL1_Continuous);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_oldTriggerSF_EL_Trigger_UP", &weight_oldTriggerSF_EL_Trigger_UP, &b_weight_oldTriggerSF_EL_Trigger_UP);
   fChain->SetBranchAddress("weight_oldTriggerSF_EL_Trigger_DOWN", &weight_oldTriggerSF_EL_Trigger_DOWN, &b_weight_oldTriggerSF_EL_Trigger_DOWN);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_UP", &weight_oldTriggerSF_MU_Trigger_STAT_UP, &b_weight_oldTriggerSF_MU_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_DOWN", &weight_oldTriggerSF_MU_Trigger_STAT_DOWN, &b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_UP", &weight_oldTriggerSF_MU_Trigger_SYST_UP, &b_weight_oldTriggerSF_MU_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_DOWN", &weight_oldTriggerSF_MU_Trigger_SYST_DOWN, &b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
   fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_isTight", &el_isTight, &b_el_isTight);
   fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
   fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
   fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
   fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthType", &el_true_firstEgMotherTruthType, &b_el_true_firstEgMotherTruthType);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthOrigin", &el_true_firstEgMotherTruthOrigin, &b_el_true_firstEgMotherTruthOrigin);
   fChain->SetBranchAddress("el_true_firstEgMotherPdgId", &el_true_firstEgMotherPdgId, &b_el_true_firstEgMotherPdgId);
   fChain->SetBranchAddress("el_true_isPrompt", &el_true_isPrompt, &b_el_true_isPrompt);
   fChain->SetBranchAddress("el_true_isChargeFl", &el_true_isChargeFl, &b_el_true_isChargeFl);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_isTight", &mu_isTight, &b_mu_isTight);
   fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
   fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
   fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
   fChain->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt, &b_mu_true_isPrompt);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_passfjvt", &jet_passfjvt, &b_jet_passfjvt);
   fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
   fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
   fChain->SetBranchAddress("jet_isTrueHS", &jet_isTrueHS, &b_jet_isTrueHS);
   fChain->SetBranchAddress("jet_truthflavExtended", &jet_truthflavExtended, &b_jet_truthflavExtended);
   fChain->SetBranchAddress("jet_isbtagged_MV2c10_60", &jet_isbtagged_MV2c10_60, &b_jet_isbtagged_MV2c10_60);
   fChain->SetBranchAddress("jet_isbtagged_MV2c10_70", &jet_isbtagged_MV2c10_70, &b_jet_isbtagged_MV2c10_70);
   fChain->SetBranchAddress("jet_isbtagged_MV2c10_77", &jet_isbtagged_MV2c10_77, &b_jet_isbtagged_MV2c10_77);
   fChain->SetBranchAddress("jet_isbtagged_MV2c10_85", &jet_isbtagged_MV2c10_85, &b_jet_isbtagged_MV2c10_85);
   fChain->SetBranchAddress("jet_tagWeightBin_MV2c10_Continuous", &jet_tagWeightBin_MV2c10_Continuous, &b_jet_tagWeightBin_MV2c10_Continuous);
   fChain->SetBranchAddress("jet_isbtagged_DL1_60", &jet_isbtagged_DL1_60, &b_jet_isbtagged_DL1_60);
   fChain->SetBranchAddress("jet_isbtagged_DL1_70", &jet_isbtagged_DL1_70, &b_jet_isbtagged_DL1_70);
   fChain->SetBranchAddress("jet_isbtagged_DL1_77", &jet_isbtagged_DL1_77, &b_jet_isbtagged_DL1_77);
   fChain->SetBranchAddress("jet_isbtagged_DL1_85", &jet_isbtagged_DL1_85, &b_jet_isbtagged_DL1_85);
   fChain->SetBranchAddress("jet_tagWeightBin_DL1_Continuous", &jet_tagWeightBin_DL1_Continuous, &b_jet_tagWeightBin_DL1_Continuous);
   fChain->SetBranchAddress("ljet_pt", &ljet_pt, &b_ljet_pt);
   fChain->SetBranchAddress("ljet_eta", &ljet_eta, &b_ljet_eta);
   fChain->SetBranchAddress("ljet_phi", &ljet_phi, &b_ljet_phi);
   fChain->SetBranchAddress("ljet_e", &ljet_e, &b_ljet_e);
   fChain->SetBranchAddress("ljet_m", &ljet_m, &b_ljet_m);
   fChain->SetBranchAddress("ljet_sd12", &ljet_sd12, &b_ljet_sd12);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0", &el_trigMatch_HLT_e60_lhmedium_nod0, &b_el_trigMatch_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("el_trigMatch_HLT_e26_lhtight_nod0_ivarloose", &el_trigMatch_HLT_e26_lhtight_nod0_ivarloose, &b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0", &el_trigMatch_HLT_e140_lhloose_nod0, &b_el_trigMatch_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu50", &mu_trigMatch_HLT_mu50, &b_mu_trigMatch_HLT_mu50);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu26_ivarmedium", &mu_trigMatch_HLT_mu26_ivarmedium, &b_mu_trigMatch_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("jet_tagWeightBin", &jet_tagWeightBin, &b_jet_tagWeightBin);
   fChain->SetBranchAddress("el_LHMedium", &el_LHMedium, &b_el_LHMedium);
   fChain->SetBranchAddress("el_LHTight", &el_LHTight, &b_el_LHTight);
   fChain->SetBranchAddress("el_isPrompt", &el_isPrompt, &b_el_isPrompt);
   fChain->SetBranchAddress("el_isoFCLoose", &el_isoFCLoose, &b_el_isoFCLoose);
   fChain->SetBranchAddress("el_isoFCTight", &el_isoFCTight, &b_el_isoFCTight);
   fChain->SetBranchAddress("el_isoGradient", &el_isoGradient, &b_el_isoGradient);
   fChain->SetBranchAddress("el_true_pdg", &el_true_pdg, &b_el_true_pdg);
   fChain->SetBranchAddress("el_true_eta", &el_true_eta, &b_el_true_eta);
   fChain->SetBranchAddress("el_true_pt", &el_true_pt, &b_el_true_pt);
   fChain->SetBranchAddress("mu_Medium", &mu_Medium, &b_mu_Medium);
   fChain->SetBranchAddress("mu_Tight", &mu_Tight, &b_mu_Tight);
   fChain->SetBranchAddress("mu_isPrompt", &mu_isPrompt, &b_mu_isPrompt);
   fChain->SetBranchAddress("mu_isoFCLoose", &mu_isoFCLoose, &b_mu_isoFCLoose);
   fChain->SetBranchAddress("mu_isoFCTight", &mu_isoFCTight, &b_mu_isoFCTight);
   fChain->SetBranchAddress("mu_isoFCTightTrackOnly", &mu_isoFCTightTrackOnly, &b_mu_isoFCTightTrackOnly);
   fChain->SetBranchAddress("mu_isoGradient", &mu_isoGradient, &b_mu_isoGradient);
   fChain->SetBranchAddress("mu_true_pdg", &mu_true_pdg, &b_mu_true_pdg);
   fChain->SetBranchAddress("mu_true_eta", &mu_true_eta, &b_mu_true_eta);
   fChain->SetBranchAddress("mu_true_pt", &mu_true_pt, &b_mu_true_pt);
   fChain->SetBranchAddress("HF_Classification", &HF_Classification, &b_HF_Classification);
   fChain->SetBranchAddress("HF_ClassificationGhost", &HF_ClassificationGhost, &b_HF_ClassificationGhost);
   fChain->SetBranchAddress("HF_SimpleClassification", &HF_SimpleClassification, &b_HF_SimpleClassification);
   fChain->SetBranchAddress("HF_SimpleClassificationGhost", &HF_SimpleClassificationGhost, &b_HF_SimpleClassificationGhost);
   fChain->SetBranchAddress("HadTop200", &HadTop200, &b_HadTop200);
   fChain->SetBranchAddress("TTbar150", &TTbar150, &b_TTbar150);
   fChain->SetBranchAddress("TopHeavyFlavorFilterFlag", &TopHeavyFlavorFilterFlag, &b_TopHeavyFlavorFilterFlag);
   fChain->SetBranchAddress("leptonType", &leptonType, &b_leptonType);
   fChain->SetBranchAddress("nBTags_60", &nBTags_60, &b_nBTags_60);
   fChain->SetBranchAddress("nBTags_70", &nBTags_70, &b_nBTags_70);
   fChain->SetBranchAddress("nBTags_77", &nBTags_77, &b_nBTags_77);
   fChain->SetBranchAddress("nBTags_85", &nBTags_85, &b_nBTags_85);
   fChain->SetBranchAddress("nBTags_DL1_60", &nBTags_DL1_60, &b_nBTags_DL1_60);
   fChain->SetBranchAddress("nBTags_DL1_70", &nBTags_DL1_70, &b_nBTags_DL1_70);
   fChain->SetBranchAddress("nBTags_DL1_77", &nBTags_DL1_77, &b_nBTags_DL1_77);
   fChain->SetBranchAddress("nBTags_DL1_85", &nBTags_DL1_85, &b_nBTags_DL1_85);
   fChain->SetBranchAddress("nBTags_MV2c10_60", &nBTags_MV2c10_60, &b_nBTags_MV2c10_60);
   fChain->SetBranchAddress("nBTags_MV2c10_70", &nBTags_MV2c10_70, &b_nBTags_MV2c10_70);
   fChain->SetBranchAddress("nBTags_MV2c10_77", &nBTags_MV2c10_77, &b_nBTags_MV2c10_77);
   fChain->SetBranchAddress("nBTags_MV2c10_85", &nBTags_MV2c10_85, &b_nBTags_MV2c10_85);
   fChain->SetBranchAddress("nElectrons", &nElectrons, &b_nElectrons);
   fChain->SetBranchAddress("nHFJets", &nHFJets, &b_nHFJets);
   fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
   fChain->SetBranchAddress("nMuons", &nMuons, &b_nMuons);
   fChain->SetBranchAddress("nPDFFlavor", &nPDFFlavor, &b_nPDFFlavor);
   fChain->SetBranchAddress("nPrimaryVtx", &nPrimaryVtx, &b_nPrimaryVtx);
   fChain->SetBranchAddress("nTaus", &nTaus, &b_nTaus);
   fChain->SetBranchAddress("truth_HDecay", &truth_HDecay, &b_truth_HDecay);
   fChain->SetBranchAddress("truth_nHiggs", &truth_nHiggs, &b_truth_nHiggs);
   fChain->SetBranchAddress("truth_nLepTop", &truth_nLepTop, &b_truth_nLepTop);
   fChain->SetBranchAddress("truth_nTop", &truth_nTop, &b_truth_nTop);
   fChain->SetBranchAddress("truth_nVectorBoson", &truth_nVectorBoson, &b_truth_nVectorBoson);
   fChain->SetBranchAddress("truth_top_dilep_filter", &truth_top_dilep_filter, &b_truth_top_dilep_filter);
   fChain->SetBranchAddress("ttbb_categories", &ttbb_categories, &b_ttbb_categories);
   fChain->SetBranchAddress("HFClassification_q1_eta", &HFClassification_q1_eta, &b_HFClassification_q1_eta);
   fChain->SetBranchAddress("HFClassification_q1_m", &HFClassification_q1_m, &b_HFClassification_q1_m);
   fChain->SetBranchAddress("HFClassification_q1_phi", &HFClassification_q1_phi, &b_HFClassification_q1_phi);
   fChain->SetBranchAddress("HFClassification_q1_pt", &HFClassification_q1_pt, &b_HFClassification_q1_pt);
   fChain->SetBranchAddress("HFClassification_q2_eta", &HFClassification_q2_eta, &b_HFClassification_q2_eta);
   fChain->SetBranchAddress("HFClassification_q2_m", &HFClassification_q2_m, &b_HFClassification_q2_m);
   fChain->SetBranchAddress("HFClassification_q2_phi", &HFClassification_q2_phi, &b_HFClassification_q2_phi);
   fChain->SetBranchAddress("HFClassification_q2_pt", &HFClassification_q2_pt, &b_HFClassification_q2_pt);
   fChain->SetBranchAddress("HFClassification_qq_dr", &HFClassification_qq_dr, &b_HFClassification_qq_dr);
   fChain->SetBranchAddress("HFClassification_qq_ht", &HFClassification_qq_ht, &b_HFClassification_qq_ht);
   fChain->SetBranchAddress("HFClassification_qq_m", &HFClassification_qq_m, &b_HFClassification_qq_m);
   fChain->SetBranchAddress("HFClassification_qq_pt", &HFClassification_qq_pt, &b_HFClassification_qq_pt);
   fChain->SetBranchAddress("truth_hadtop_pt", &truth_hadtop_pt, &b_truth_hadtop_pt);
   fChain->SetBranchAddress("truth_higgs_eta", &truth_higgs_eta, &b_truth_higgs_eta);
   fChain->SetBranchAddress("truth_higgs_pt", &truth_higgs_pt, &b_truth_higgs_pt);
   fChain->SetBranchAddress("truth_tbar_pt", &truth_tbar_pt, &b_truth_tbar_pt);
   fChain->SetBranchAddress("truth_top_pt", &truth_top_pt, &b_truth_top_pt);
   fChain->SetBranchAddress("truth_ttbar_pt", &truth_ttbar_pt, &b_truth_ttbar_pt);
   fChain->SetBranchAddress("truth_hadron_pt", &truth_hadron_pt, &b_truth_hadron_pt);
   fChain->SetBranchAddress("truth_hadron_eta", &truth_hadron_eta, &b_truth_hadron_eta);
   fChain->SetBranchAddress("truth_hadron_phi", &truth_hadron_phi, &b_truth_hadron_phi);
   fChain->SetBranchAddress("truth_hadron_m", &truth_hadron_m, &b_truth_hadron_m);
   fChain->SetBranchAddress("truth_hadron_pdgid", &truth_hadron_pdgid, &b_truth_hadron_pdgid);
   fChain->SetBranchAddress("truth_hadron_status", &truth_hadron_status, &b_truth_hadron_status);
   fChain->SetBranchAddress("truth_hadron_barcode", &truth_hadron_barcode, &b_truth_hadron_barcode);
   fChain->SetBranchAddress("truth_hadron_TopHadronOriginFlag", &truth_hadron_TopHadronOriginFlag, &b_truth_hadron_TopHadronOriginFlag);
   fChain->SetBranchAddress("truth_hadron_type", &truth_hadron_type, &b_truth_hadron_type);
   fChain->SetBranchAddress("truth_hadron_final", &truth_hadron_final, &b_truth_hadron_final);
   fChain->SetBranchAddress("truth_hadron_initial", &truth_hadron_initial, &b_truth_hadron_initial);
   fChain->SetBranchAddress("truth_pt", &truth_pt, &b_truth_pt);
   fChain->SetBranchAddress("truth_eta", &truth_eta, &b_truth_eta);
   fChain->SetBranchAddress("truth_phi", &truth_phi, &b_truth_phi);
   fChain->SetBranchAddress("truth_m", &truth_m, &b_truth_m);
   fChain->SetBranchAddress("truth_pdgid", &truth_pdgid, &b_truth_pdgid);
   fChain->SetBranchAddress("truth_status", &truth_status, &b_truth_status);
   fChain->SetBranchAddress("truth_barcode", &truth_barcode, &b_truth_barcode);
   fChain->SetBranchAddress("truth_tthbb_info", &truth_tthbb_info, &b_truth_tthbb_info);
   fChain->SetBranchAddress("truth_jet_pt", &truth_jet_pt, &b_truth_jet_pt);
   fChain->SetBranchAddress("truth_jet_eta", &truth_jet_eta, &b_truth_jet_eta);
   fChain->SetBranchAddress("truth_jet_phi", &truth_jet_phi, &b_truth_jet_phi);
   fChain->SetBranchAddress("truth_jet_m", &truth_jet_m, &b_truth_jet_m);
   fChain->SetBranchAddress("truth_jet_flav", &truth_jet_flav, &b_truth_jet_flav);
   fChain->SetBranchAddress("truth_jet_id", &truth_jet_id, &b_truth_jet_id);
   fChain->SetBranchAddress("truth_jet_count", &truth_jet_count, &b_truth_jet_count);
   Notify();
}

Bool_t mbbtool::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void mbbtool::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t mbbtool::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef mbbtool_cxx
