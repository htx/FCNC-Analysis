#include "inclusions.h"
#include "compare_yields.cxx"
using namespace std;
void run(void)
{
    TFile *file1 = new TFile("Data_blinded_debug_allbins_singletop_h120/Histograms/c1l4jex3bex_postFit.root");
    TFile *file2 = new TFile("Data_blinded_debug_allbins_singletop_h120/Histograms/c1l5jex3bex_postFit.root");
    TFile *file3 = new TFile("Data_blinded_debug_allbins_singletop_h120/Histograms/c1l6jin3bex_postFit.root");
    TFile *file4 = new TFile("Data_blinded_debug_allbins_singletop_h120/Histograms/c1l6jin4bin_postFit.root");

    TFile *file5 = new TFile("Data_blinded_debug_allbins_singletop_no2bloose_h120/Histograms/c1l4jex3bex_postFit.root");
    TFile *file6 = new TFile("Data_blinded_debug_allbins_singletop_no2bloose_h120/Histograms/c1l5jex3bex_postFit.root");
    TFile *file7 = new TFile("Data_blinded_debug_allbins_singletop_no2bloose_h120/Histograms/c1l6jin3bex_postFit.root");
    TFile *file8 = new TFile("Data_blinded_debug_allbins_singletop_no2bloose_h120/Histograms/c1l6jin4bin_postFit.root");

   	compare_yields(file1,file2,file3,file4,file5,file6,file7,file8);


}
