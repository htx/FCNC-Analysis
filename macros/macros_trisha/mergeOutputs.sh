#!/bin/bash

INDIR=$1
CURDIR=$PWD 

cd ${INDIR}
mkdir MergedFiles

hadd MergedFiles/outVLQAnalysis_Data_nominal.root HistFiles/outVLQAnalysis_Data_*.root
hadd MergedFiles/outVLQAnalysis_QCD_nominal.root HistFiles/outVLQAnalysis_QCD_*.root
hadd MergedFiles/outVLQAnalysis_ttbarlight_nominal.root HistFiles/outVLQAnalysis_ttbarlight_*.root
hadd MergedFiles/outVLQAnalysis_ttbarcc_nominal.root HistFiles/outVLQAnalysis_ttbarcc_*.root
hadd MergedFiles/outVLQAnalysis_ttbarbb_nominal.root HistFiles/outVLQAnalysis_ttbarbb_*.root
hadd MergedFiles/outVLQAnalysis_singletop_nominal.root HistFiles/outVLQAnalysis_Singletop_*.root
hadd MergedFiles/outVLQAnalysis_Wjets22_nominal.root HistFiles/outVLQAnalysis_W+jets22*_*.root
hadd MergedFiles/outVLQAnalysis_Zjets22_nominal.root HistFiles/outVLQAnalysis_Z+jets22*_*.root
hadd MergedFiles/outVLQAnalysis_others_nominal.root HistFiles/outVLQAnalysis_topEW_*.root HistFiles/outVLQAnalysis_ttH_*.root HistFiles/outVLQAnalysis_Dibosons_*.root HistFiles/outVLQAnalysis_4tops_*.root

cd ${CURDIR}
