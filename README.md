Checking Out
============

To set up the release

    setupATLAS
    rcSetup Base,2.4.24  //just to set up a new root version

Otherwise replace with: 
    
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    rcSetup Base,2.4.24

Then checkout all relevant packages
    
    git clone ssh://git@gitlab.cern.ch:7999/htx/IFAETopFramework.git
    cd IFAETopFramework && git checkout Hplus_branch
    
    git clone ssh://git@gitlab.cern.ch:7999/htx/IFAEReweightingTools.git
    cd IFAEReweightingTools && git checkout IFAEReweightingTools-00-01-06-07 && cd ..

    git clone ssh://git@gitlab.cern.ch:7999/htx/FCNC-Analysis.git
     
    git clone ssh://git@gitlab.cern.ch:7999/htx/BtaggingTRFandRW.git 
 
The compilation of the packages (can be a few minutes long due to the TRF package) is done as follows::

    rc find_packages
    rc clean
    rc compile

An example line of code to execute is::

    FCNCAnalysis --outputFile=test_data.root \ 
    --inputFile=/nfs/at3/scratch2/orlando/FCNC-Full-Run2/prod-june-2020/s/af2_folder/user.orlando.mc16_13TeV.410470.PhPy8EG_ttbar_lep.TOPQ1.e6337a875r9364p4031.TTHbb30062020-v0_1l_sys_out_root//*root* \ 
    --textFileList=false --sampleName=ttbar --sampleID=410470. --inputTree=nominal_Loose \ 
    --isData=false --dumpHistos=true --dumpTreeForIso=True \ 
    --LepIsoCut=Nominal --nEvents=5000 --sampleDat=samples_info_FCNC_prod-Jun-2020_nominal_af2.dat

**Important** currently ROOTCORE is not available anymore on at304. 

**Important** currently you may have problems in compiling BtaggingTRFandRW, solution is 

    cd BtaggingTRFandRW
    rm -r .svn
    rm -r */.svn
    cd ../



**Processing a new production**

When processing a new production, assuming that no new samples are added you have to follow the following steps

* Produce a new sample_info.dat corresponding  to the new production, this can be done with python/ProduceSampleDATfile.py, you just need to pass the new input folder where the production is being stored. This will produce a text file with the sum of weights and cross sections times filter efficiency. 

* Move the sample_info.dat produced in the above step in the folder data and pass it to FCNC_Options ("sampleDat")


**Processing a new sample**

When processing a new production, assuming that no new samples are added you have to follow the following steps

* The cross section for the new sample must be added here data/XSection-MC15-13TeV.data

* The new samples must be added in  python/Analysis_Samples.py and you should make sure to call them in the main script for the job submission python/Submit_Analysis.py

* You will need to produce the corresponding sample_info.dat for the new samples


**Currently supported options**

* useLeptonsSF: apply lepton scale factors to output histograms

* useBtagSF: apply b-tagging scale factors to output histograms

* recomputeBtagSF: recomput offline the b-tagging scale factors 

* usePileUpWeight: apply pile-up wieghts scale factors to output histograms

* dumpHistos: write output histogram files

* dumpTree: write output ntuple files

* dumpIsoTree: add lepton isolation info in the output tree - specific option for optimisation studies 

* dumpDLTree: prepare dedicated deep learning training tree with minimal set of information

* dumpOverlapTree: prepare tree for overlap studies - relevant for combination studies

* doTruthAnalysis: do truth level analysis 

* doTRF: run in TRF mode

* doPerBinTRF: run in TRF mode but now correcting the output variables bin by bin

* recomputeTRF: recomput TRF weights offline

* applyMetMtwCuts: apply MET and MET+MTW cuts

* invertMetMtwCuts: invert MET and MET+MTW cuts - relevant for specific studies, e.g. QCD studies

* multipleVariablesWithUncertainties: add multiple variables in the output histogram with systematic uncertainties added - relevant for control plots inclusing systematic uncertainties

* doCutFlow: print cut-flow

* doLowBRegions: write histograms corresponding to 0b/1b regions - relevant only when validating QCD and V+jets modelling 

* doSplitEMu: split the histograms in electron and muon channels 

* doSumRegions: do inclusive regions if they are not already defined in the selector class - usually not needed

* scaleTtbarHtSlices: rescale the ttbar samples to match the slicing strategy - to be updated for Release 21

* maxb: max number of allowed b-jets in the event - usually not needed

* jetsPtCut: pt cut on the jets

* jetsEtaCut: eta cut on the jets

* lepPtCut: pt cut on the leptons 

* btagOP: b-tagging operating point, allowed values are 60, 70, 77, 85

* btagAlg: btagging algorithm, allowed values are DL1 and MV2c10

* LepIsoCut: lepton isolation cut - only relevant when running with dumpTreeForIso ON

* TRFCDIPath: path to the CDI file 

* sampleDat: text file having all needed normalisation factors for the samples

* filterType: slicing scheme for ttbar - to be updated fr Release 21. 

* HFFilter: heavy flavour filter.
