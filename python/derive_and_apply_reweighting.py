#!/bin/env python

from array import array
import os
import sys
import ROOT as R
import numpy as np
R.gROOT.SetBatch(1)

from ROOT import TKDE
from ROOT import TCanvas
from ROOT import TH1F
from ROOT import TF1
from ROOT import TFile

# Global config 
meff_n_bin = 30
meff_min_bin = 0
meff_max_bin = 1500

meff_cut = meff_min_bin

def process_tree_for_histo(input_path,input_type,tree_name,is_mc,n_jets,output_filename,btag_cut,jet_coll,alt,debug=False):
    print('In process_tree_for_histo')

    input_data = TH1F('input_'+input_type,'input_'+input_type,meff_n_bin,meff_min_bin,meff_max_bin)
    input_data.Sumw2()

    non_ttbar = ['topEW','ttH','ttHtH','Wjets22light','Zjets22light','Dibosons','Singletop']
    if jet_coll == 'EMTopo':    
        ttbar = ['ttbarbb'+alt,'ttbarcc'+alt,'ttbarlight'+alt]
    elif jet_coll == 'PFlow':
          ttbar = ['ttbarbb'+alt,'ttbarcc'+alt,'ttbarlight'+alt,'Wtocb'+alt]
    data = ['Data']

    Samples=[]
    if input_type == 'ttbar':
        Samples = ttbar
    elif input_type == 'non_ttbar':
          Samples = non_ttbar
    elif input_type == 'data':
          Samples = data

    for sample in Samples :
        input_filename = input_path+sample+'.root'
        print(input_filename)
        input_file = R.TFile.Open(input_filename)
        input_tree = input_file.Get(tree_name)
        meff_vec = R.std.vector('double')()
        weight_vec = R.std.vector('double')()
        print('Entering event loop now')
        for event in input_tree:
            if event.jets_n == n_jets and event.jet0_btagw_bord >= btag_cut and event.jet1_btagw_bord >= btag_cut and event.jet2_btagw_bord < btag_cut :  
              if event.meff > meff_cut:
                  if debug:
                      print('meff ',(event.meff))
                  meff_vec.push_back(event.meff)
                  if is_mc:
                      weight_vec.push_back(event.nomWeight_weight_btag*event.nomWeight_weight_jvt*event.nomWeight_weight_leptonSF*event.nomWeight_weight_mc*event.nomWeight_weight_norm*event.nomWeight_weight_pu)
                      input_data.Fill(event.meff,event.nomWeight_weight_btag*event.nomWeight_weight_jvt*event.nomWeight_weight_leptonSF*event.nomWeight_weight_mc*event.nomWeight_weight_norm*event.nomWeight_weight_pu)
                  else:
                      input_data.Fill(event.meff)
                      weight_vec.push_back(1)
        input_file.Close()
        print('Moving to next input sample')

    print('Saving histogram in '+output_filename)
    output_file = TFile(output_filename,'recreate')
    input_data.Write()
    output_file.Close()

def derive_fit_function(input_mc_ttbar,input_mc_nonttbar,input_data,output_filename): 
    print('In derive_fit_function')
    output_file = TFile(output_filename,'recreate')
    input_mc_ttbar_histo = input_mc_ttbar.Get('input_ttbar')
    input_mc_nonttbar_histo = input_mc_nonttbar.Get('input_non_ttbar')
    input_data_histo = input_data.Get('input_data')

    # Norm hists to lumi/unity 
    input_mc_ttbar_histo.Scale(139000.)
    #input_mc_ttbar_histo.Scale(1./input_mc_ttbar_histo.Integral())
    input_mc_nonttbar_histo.Scale(139000.)
    #input_mc_nonttbar_histo.Scale(1./input_mc_nonttbar_histo.Integral())
    #input_data_histo.Scale(1./input_data_histo.Integral())

    # Ratio between unity normalised histograms, no normalisation correction
    ratio_histo = input_data_histo.Clone('ratio_histo')
    ratio_histo.Add(input_mc_nonttbar_histo,-1)
    substracion_histo = ratio_histo.Clone('data_minus_nonttbar') 
    ratio_histo.Divide(input_mc_ttbar_histo)
    # Choose fit function
    f1 = TF1("f1","[0]+[1]*x+[2]*x*x",meff_min_bin,meff_max_bin) #default:quadratic
    f2 = TF1("f2","[0]+[1]*x",meff_min_bin,meff_max_bin) #linear
    f3 = TF1("f3","[0]+[1]*x+[2]*x*x+[3]*x*x*x",meff_min_bin,meff_max_bin) #cubic
    result = ratio_histo.Fit(f1,"NSQ","",meff_min_bin,meff_max_bin)
    result2 = ratio_histo.Fit(f2,"NSQ","",meff_min_bin,meff_max_bin)
    result3 = ratio_histo.Fit(f3,"NSQ","",meff_min_bin,meff_max_bin)
    m_cov = result.GetCovarianceMatrix()
    #print(m_cov.Print("V"))
    param = []
    param.append(f1.GetParameter(0))
    param.append(f1.GetParameter(1))
    param.append(f1.GetParameter(2))
    #print(param[0],param[1],param[2])

    #save TMatrixSym info into 2D array for further proccessing
    a_cov = [
      [m_cov[0][0],m_cov[0][1],m_cov[0][2]],
      [m_cov[1][0],m_cov[1][1],m_cov[1][2]],
      [m_cov[2][0],m_cov[2][1],m_cov[2][2]]
    ] 
    #print(a_cov[0][0],a_cov[0][1],a_cov[0][2])
    #print(a_cov[1][0],a_cov[1][1],a_cov[1][2])
    #print(a_cov[2][0],a_cov[2][1],a_cov[2][2])

    #derive stat uncertainty from fit
    evalues, evectors = np.linalg.eig(a_cov)
    paramname = ["A","B","C"]
    Evariations = []
    print("Param\tNominal+Variation")
    for parname,val,vec in zip(paramname,evalues,np.transpose(evectors)):
        aux = vec.dot(np.sqrt(val))
        print(parname)
        for par,var in zip(param,aux):
            print(par,"+",var)
        Evariations.append(aux)

    #defining variations functions
    var_A = []
    var_A = Evariations[0]
    fA_up = TF1("fA_up","[0]+[1]*x+[2]*x*x",meff_min_bin,meff_max_bin)
    fA_up.SetParameters(param[0]+var_A[0],param[1]+var_A[1],param[2]+var_A[2])
    fA_down = TF1("fA_down","[0]+[1]*x+[2]*x*x",meff_min_bin,meff_max_bin)
    fA_down.SetParameters(param[0]-var_A[0],param[1]-var_A[1],param[2]-var_A[2])
    #print(param[0],"+/-",var_A[0],param[1],"+/-",var_A[1],param[2],"+/-",var_A[2])

    var_B = []
    var_B = Evariations[1]
    fB_up = TF1("fB_up","[0]+[1]*x+[2]*x*x",meff_min_bin,meff_max_bin)
    fB_up.SetParameters(param[0]+var_B[0],param[1]+var_B[1],param[2]+var_B[2])
    fB_down = TF1("fB_down","[0]+[1]*x+[2]*x*x",meff_min_bin,meff_max_bin)
    fB_down.SetParameters(param[0]-var_B[0],param[1]-var_B[1],param[2]-var_B[2])
    #print(param[0],"+/-",var_B[0],param[1],"+/-",var_B[1],param[2],"+/-",var_B[2])

    var_C = []
    var_C = Evariations[2]
    fC_up = TF1("fC_up","[0]+[1]*x+[2]*x*x",meff_min_bin,meff_max_bin)
    fC_up.SetParameters(param[0]+var_C[0],param[1]+var_C[1],param[2]+var_C[2])
    fC_down = TF1("fC_down","[0]+[1]*x+[2]*x*x",meff_min_bin,meff_max_bin)
    fC_down.SetParameters(param[0]-var_C[0],param[1]-var_C[1],param[2]-var_C[2])
    #print(param[0],"+/-",var_C[0],param[1],"+/-",var_C[1],param[2],"+/-",var_C[2])

    ratio_histo.Write()
    input_mc_ttbar_histo.Write()
    input_mc_nonttbar_histo.Write()
    input_data_histo.Write()
    substracion_histo.Write()
    f1.Write()
    f2.Write()
    f3.Write()
    fA_up.Write()
    fA_down.Write()
    fB_up.Write()
    fB_down.Write()
    fC_up.Write()
    fC_down.Write()
    output_file.Close()

def process_actual_output_function(func_4j,func_5j,func_6j,filename,tree_name,var_name,function_name,debug=False): 
    print('In process_actual_output_functions')
    input_file = R.TFile.Open(filename,'update')
    input_tree = input_file.Get(tree_name)
    rew_func_4j  = array('d',[0])
    rew_func_5j  = array('d',[0])
    rew_func_6j  = array('d',[0])
    branch_new_func_4j = input_tree.Branch('branch_rew_func_4j'+var_name+function_name,rew_func_4j,'branch_rew_func_4j'+var_name+'/D')
    branch_new_func_5j = input_tree.Branch('branch_rew_func_5j'+var_name+function_name,rew_func_5j,'branch_rew_func_5j'+var_name+'/D')
    branch_new_func_6j = input_tree.Branch('branch_rew_func_6j'+var_name+function_name,rew_func_6j,'branch_rew_func_6j'+var_name+'/D')
    index = 0
    tot_entries = float(input_tree.GetEntries())
    for event in input_tree :
        if debug == True: 
            print('Processing event %i of %f', (index,tot_entries))
        rew_func_4j[0] = func_4j.Eval(event.meff)
        rew_func_5j[0] = func_5j.Eval(event.meff)
        rew_func_6j[0] = func_6j.Eval(event.meff)
        index = index +1
        branch_new_func_4j.Fill()
        branch_new_func_5j.Fill()
        branch_new_func_6j.Fill()
        if  index > tot_entries:
            break
    input_file.Write()
    input_file.Close()

def process_actual_output_alt_sample(ratio_4j,ratio_5j,ratio_6j,filename,tree_name,alt_name,debug=False): 
    print('In process_actual_output_alt_ttbar_sample')
    input_file = R.TFile.Open(filename,'update')
    input_tree = input_file.Get(tree_name)
    rew_func_4j  = array('d',[0])
    rew_func_5j  = array('d',[0])
    rew_func_6j  = array('d',[0])
    branch_new_func_4j = input_tree.Branch('branch_rew_func_4j_'+alt_name,rew_func_4j,'branch_rew_func_4j_'+alt_name+'/D')
    branch_new_func_5j = input_tree.Branch('branch_rew_func_5j_'+alt_name,rew_func_5j,'branch_rew_func_5j_'+alt_name+'/D')
    branch_new_func_6j = input_tree.Branch('branch_rew_func_6j_'+alt_name,rew_func_6j,'branch_rew_func_6j_'+alt_name+'/D')
 
    index = 0
    tot_entries = float(input_tree.GetEntries())
    for event in input_tree :
        if debug == True: 
            print('Processing event %i of %f', (index,tot_entries))
        bin_4j = ratio_4j.GetXaxis().FindBin(event.meff)
        rew_func_4j[0] = ratio_4j.GetBinContent(bin_4j)
        bin_5j = ratio_5j.GetXaxis().FindBin(event.meff)
        rew_func_5j[0] = ratio_5j.GetBinContent(bin_5j)
        bin_6j = ratio_6j.GetXaxis().FindBin(event.meff)
        rew_func_6j[0] = ratio_6j.GetBinContent(bin_6j)
        index = index +1
        branch_new_func_4j.Fill()
        branch_new_func_5j.Fill()
        branch_new_func_6j.Fill()
        if  index > tot_entries:
            break
    input_file.Write()
    input_file.Close()

def main():

    run_mode = sys.argv[1] #1) to create input histograms, 2) to derive fit function, 3) to create and store info in branch 
    jet_collection = sys.argv[2] # EMTopo or PFlow jets
    systematics = sys.argv[3] # if False->branches with weights are stored only on nominal ttbar samples, if True->they are also stored on systematic samples 
    ttbar_sample = sys.argv[4] # if Nominal doesn't change anything if Alternative ir over ttbar alternative samples 
    tree_name = 'tree'

    print('Options ')
    print('run_mode ', run_mode)
    print('Jet Collection ', jet_collection)
    print('Systematics ', systematics)
    print('ttbar sample type ', ttbar_sample)

    input_path = ''
    btag_cut = 0.0
    if jet_collection == 'EMTopo':
        btag_cut = 2.74
        input_path = '/nfs/at3/scratch2/mdaneri/Fit_inputs_2020_DNN/' # emtopo nom+syst
    if jet_collection == 'PFlow':
        btag_cut = 4.31
        input_path = '/nfs/at3/scratch2/mdaneri/Fit_inputs_pflow_oct2020/' # pflow nominal
    print('The btag cut is ', btag_cut)

    if run_mode == '1':
	print('Running on mode 1, creating meff histrograms from samples')

        if ttbar_sample == 'Nominal' :
	    print('Deriving histos for 4j2b1loose')
	    process_tree_for_histo(input_path,'ttbar',tree_name,True,4,'h_meff_ttbar_4j2b1loose.root',btag_cut,jet_collection,'')
	    process_tree_for_histo(input_path,'non_ttbar',tree_name,True,4,'h_meff_nonttbar_4j2b1loose.root',btag_cut,jet_collection,'')
	    process_tree_for_histo(input_path,'data',tree_name,False,4,'h_meff_data_4j2b1loose.root',btag_cut,jet_collection,'')

	    print('Deriving histos for 5j2b1loose')
	    process_tree_for_histo(input_path,'ttbar',tree_name,True,5,'h_meff_ttbar_5j2b1loose.root',btag_cut,jet_collection,'')
	    process_tree_for_histo(input_path,'non_ttbar',tree_name,True,5,'h_meff_nonttbar_5j2b1loose.root',btag_cut,jet_collection,'')
	    process_tree_for_histo(input_path,'data',tree_name,False,5,'h_meff_data_5j2b1loose.root',btag_cut,jet_collection,'')

	    print('Deriving histos for 6j2b1loose')
	    process_tree_for_histo(input_path,'ttbar',tree_name,True,6,'h_meff_ttbar_6j2b1loose.root',btag_cut,jet_collection,'')
            process_tree_for_histo(input_path,'non_ttbar',tree_name,True,6,'h_meff_nonttbar_6j2b1loose.root',btag_cut,jet_collection,'')
	    process_tree_for_histo(input_path,'data',tree_name,False,6,'h_meff_data_6j2b1loose.root',btag_cut,jet_collection,'')

        if ttbar_sample == 'Alternative' :
            #noShWMCPy8
            process_tree_for_histo(input_path,'ttbar',tree_name,True,4,'h_meff_ttbarnoShWMCPy8_4j2b1loose.root',btag_cut,jet_collection,'noShWMCPy8')
            process_tree_for_histo(input_path,'ttbar',tree_name,True,5,'h_meff_ttbarnoShWMCPy8_5j2b1loose.root',btag_cut,jet_collection,'noShWMCPy8')
            process_tree_for_histo(input_path,'ttbar',tree_name,True,6,'h_meff_ttbarnoShWMCPy8_6j2b1loose.root',btag_cut,jet_collection,'noShWMCPy8')
            #PowH7
            process_tree_for_histo(input_path,'ttbar',tree_name,True,4,'h_meff_ttbarPowH7_4j2b1loose.root',btag_cut,jet_collection,'PowH7')
            process_tree_for_histo(input_path,'ttbar',tree_name,True,5,'h_meff_ttbarPowH7_5j2b1loose.root',btag_cut,jet_collection,'PowH7')
            process_tree_for_histo(input_path,'ttbar',tree_name,True,6,'h_meff_ttbarPowH7_6j2b1loose.root',btag_cut,jet_collection,'PowH7')

    if run_mode == '2':
	print('Running on mode 2, calculating weight function')
 
	file_nonttbar_load_4j = TFile("h_meff_nonttbar_4j2b1loose.root")
	file_nonttbar_load_5j = TFile("h_meff_nonttbar_5j2b1loose.root")
	file_nonttbar_load_6j = TFile("h_meff_nonttbar_6j2b1loose.root")

	file_nonttbar_load_4j_alt2 = TFile("h_meff_nonttbar_4j2b1loose.root")
	file_nonttbar_load_5j_alt2 = TFile("h_meff_nonttbar_5j2b1loose.root")
	file_nonttbar_load_6j_alt2 = TFile("h_meff_nonttbar_6j2b1loose.root")

	file_data_load_4j = TFile("h_meff_data_4j2b1loose.root")
	file_data_load_5j = TFile("h_meff_data_5j2b1loose.root")
	file_data_load_6j = TFile("h_meff_data_6j2b1loose.root")
    
        if ttbar_sample == 'Nominal' :
	    file_ttbar_load_4j = TFile("h_meff_ttbar_4j2b1loose.root")
 	    file_ttbar_load_5j = TFile("h_meff_ttbar_5j2b1loose.root")
 	    file_ttbar_load_6j = TFile("h_meff_ttbar_6j2b1loose.root")
        
            derive_fit_function(file_ttbar_load_4j,file_nonttbar_load_4j,file_data_load_4j,'output_funct_4j2b1loose.root')
            derive_fit_function(file_ttbar_load_5j,file_nonttbar_load_5j,file_data_load_5j,'output_funct_5j2b1loose.root')
            derive_fit_function(file_ttbar_load_6j,file_nonttbar_load_6j,file_data_load_6j,'output_funct_6j2b1loose.root')

        if ttbar_sample == 'Alternative' :
	    file_ttbar_alt1_load_4j = TFile("h_meff_ttbarnoShWMCPy8_4j2b1loose.root")
 	    file_ttbar_alt1_load_5j = TFile("h_meff_ttbarnoShWMCPy8_5j2b1loose.root")
 	    file_ttbar_alt1_load_6j = TFile("h_meff_ttbarnoShWMCPy8_6j2b1loose.root")
        
            derive_fit_function(file_ttbar_alt1_load_4j,file_nonttbar_load_4j,file_data_load_4j,'outputnoShWMCPy8_funct_4j2b1loose.root')
            derive_fit_function(file_ttbar_alt1_load_5j,file_nonttbar_load_5j,file_data_load_5j,'outputnoShWMCPy8_funct_5j2b1loose.root')
            derive_fit_function(file_ttbar_alt1_load_6j,file_nonttbar_load_6j,file_data_load_6j,'outputnoShWMCPy8_funct_6j2b1loose.root')

	    file_ttbar_alt2_load_4j = TFile("h_meff_ttbarPowH7_4j2b1loose.root")
 	    file_ttbar_alt2_load_5j = TFile("h_meff_ttbarPowH7_5j2b1loose.root")
 	    file_ttbar_alt2_load_6j = TFile("h_meff_ttbarPowH7_6j2b1loose.root")
        
            derive_fit_function(file_ttbar_alt2_load_4j,file_nonttbar_load_4j_alt2,file_data_load_4j,'outputPowH7_funct_4j2b1loose.root')
            derive_fit_function(file_ttbar_alt2_load_5j,file_nonttbar_load_5j_alt2,file_data_load_5j,'outputPowH7_funct_5j2b1loose.root')
            derive_fit_function(file_ttbar_alt2_load_6j,file_nonttbar_load_6j_alt2,file_data_load_6j,'outputPowH7_funct_6j2b1loose.root')

    if run_mode == '3':
	print('Running on mode 3, saving weights on branches on ttbar samples')

	file_function_load_4j = TFile("output_funct_4j2b1loose.root")
	function_4j = file_function_load_4j.Get("f1")
	ratio_histo_4j = file_function_load_4j.Get("ratio_histo")
	function_4j_linear = file_function_load_4j.Get("f2")
	function_4j_cubic = file_function_load_4j.Get("f3")
	function_4j_varA_up = file_function_load_4j.Get("fA_up")
	function_4j_varA_down = file_function_load_4j.Get("fA_down")
	function_4j_varB_up = file_function_load_4j.Get("fB_up")
	function_4j_varB_down = file_function_load_4j.Get("fB_down")
	function_4j_varC_up = file_function_load_4j.Get("fC_up")
	function_4j_varC_down = file_function_load_4j.Get("fC_down")

	file_function_load_5j = TFile("output_funct_5j2b1loose.root")	
	function_5j = file_function_load_5j.Get("f1")
	ratio_histo_5j = file_function_load_5j.Get("ratio_histo")
	function_5j_linear = file_function_load_5j.Get("f2")
	function_5j_cubic = file_function_load_5j.Get("f3")
	function_5j_varA_up = file_function_load_5j.Get("fA_up")
	function_5j_varA_down = file_function_load_5j.Get("fA_down")
	function_5j_varB_up = file_function_load_5j.Get("fB_up")
	function_5j_varB_down = file_function_load_5j.Get("fB_down")
	function_5j_varC_up = file_function_load_5j.Get("fC_up")
	function_5j_varC_down = file_function_load_5j.Get("fC_down")

	file_function_load_6j = TFile("output_funct_6j2b1loose.root")
	ratio_histo_6j = file_function_load_6j.Get("ratio_histo")
	function_6j = file_function_load_6j.Get("f1")
	function_6j_linear = file_function_load_6j.Get("f2")
	function_6j_cubic = file_function_load_6j.Get("f3")
	function_6j_varA_up = file_function_load_6j.Get("fA_up")
	function_6j_varA_down = file_function_load_6j.Get("fA_down")
	function_6j_varB_up = file_function_load_6j.Get("fB_up")
	function_6j_varB_down = file_function_load_6j.Get("fB_down")
	function_6j_varC_up = file_function_load_6j.Get("fC_up")
	function_6j_varC_down = file_function_load_6j.Get("fC_down")

	if ttbar_sample == 'Nominal' and systematics == 'False':
            # store nominal weight
            process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarbb.root',tree_name,'','')
            process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarcc.root',tree_name,'','')
            process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarlight.root',tree_name,'','')
            if jet_collection == 'PFlow':
                process_actual_output_function(function_4j,function_5j,function_6j,input_path+'Wtocb.root',tree_name,'','')

            # store nominal weight for linear fit function
            process_actual_output_function(function_4j_linear,function_5j_linear,function_6j_linear,input_path+'ttbarbb.root',tree_name,'','_Linear')
            process_actual_output_function(function_4j_linear,function_5j_linear,function_6j_linear,input_path+'ttbarcc.root',tree_name,'','_Linear')
            process_actual_output_function(function_4j_linear,function_5j_linear,function_6j_linear,input_path+'ttbarlight.root',tree_name,'','_Linear')

            # store nominal weight for cubic fit function
            process_actual_output_function(function_4j_cubic,function_5j_cubic,function_6j_cubic,input_path+'ttbarbb.root',tree_name,'','_Cubic')
            process_actual_output_function(function_4j_cubic,function_5j_cubic,function_6j_cubic,input_path+'ttbarcc.root',tree_name,'','_Cubic')
            process_actual_output_function(function_4j_cubic,function_5j_cubic,function_6j_cubic,input_path+'ttbarlight.root',tree_name,'','_Cubic')

            # store weight variations
            process_actual_output_function(function_4j_varA_up,function_5j_varA_up,function_6j_varA_up,input_path+'ttbarbb.root',tree_name,'_varA_up','')
            process_actual_output_function(function_4j_varA_up,function_5j_varA_up,function_6j_varA_up,input_path+'ttbarcc.root',tree_name,'_varA_up','')
            process_actual_output_function(function_4j_varA_up,function_5j_varA_up,function_6j_varA_up,input_path+'ttbarlight.root',tree_name,'_varA_up','')
            process_actual_output_function(function_4j_varA_down,function_5j_varA_down,function_6j_varA_down,input_path+'ttbarbb.root',tree_name,'_varA_down','')
            process_actual_output_function(function_4j_varA_down,function_5j_varA_down,function_6j_varA_down,input_path+'ttbarcc.root',tree_name,'_varA_down','')
            process_actual_output_function(function_4j_varA_down,function_5j_varA_down,function_6j_varA_down,input_path+'ttbarlight.root',tree_name,'_varA_down','')

            process_actual_output_function(function_4j_varB_up,function_5j_varB_up,function_6j_varB_up,input_path+'ttbarbb.root',tree_name,'_varB_up','')
            process_actual_output_function(function_4j_varB_up,function_5j_varB_up,function_6j_varB_up,input_path+'ttbarcc.root',tree_name,'_varB_up','')
            process_actual_output_function(function_4j_varB_up,function_5j_varB_up,function_6j_varB_up,input_path+'ttbarlight.root',tree_name,'_varB_up','')
            process_actual_output_function(function_4j_varB_down,function_5j_varB_down,function_6j_varB_down,input_path+'ttbarbb.root',tree_name,'_varB_down','')
            process_actual_output_function(function_4j_varB_down,function_5j_varB_down,function_6j_varB_down,input_path+'ttbarcc.root',tree_name,'_varB_down','')
            process_actual_output_function(function_4j_varB_down,function_5j_varB_down,function_6j_varB_down,input_path+'ttbarlight.root',tree_name,'_varB_down','')

            process_actual_output_function(function_4j_varC_up,function_5j_varC_up,function_6j_varC_up,input_path+'ttbarbb.root',tree_name,'_varC_up','')
            process_actual_output_function(function_4j_varC_up,function_5j_varC_up,function_6j_varC_up,input_path+'ttbarcc.root',tree_name,'_varC_up','')
            process_actual_output_function(function_4j_varC_up,function_5j_varC_up,function_6j_varC_up,input_path+'ttbarlight.root',tree_name,'_varC_up','')
            process_actual_output_function(function_4j_varC_down,function_5j_varC_down,function_6j_varC_down,input_path+'ttbarbb.root',tree_name,'_varC_down','')
            process_actual_output_function(function_4j_varC_down,function_5j_varC_down,function_6j_varC_down,input_path+'ttbarcc.root',tree_name,'_varC_down','')
            process_actual_output_function(function_4j_varC_down,function_5j_varC_down,function_6j_varC_down,input_path+'ttbarlight.root',tree_name,'_varC_down','')


        if ttbar_sample == 'Nominal' and systematics == 'True':

            Syst = ['','_CategoryReduction_JET_BJES_Response__1down_Loose','_CategoryReduction_JET_BJES_Response__1up_Loose','_CategoryReduction_JET_EffectiveNP_Detector1__1down_Loose','_CategoryReduction_JET_EffectiveNP_Detector1__1up_Loose','_CategoryReduction_JET_EffectiveNP_Detector2__1down_Loose','_CategoryReduction_JET_EffectiveNP_Detector2__1up_Loose','_CategoryReduction_JET_EffectiveNP_Mixed1__1down_Loose','_CategoryReduction_JET_EffectiveNP_Mixed1__1up_Loose','_CategoryReduction_JET_EffectiveNP_Mixed2__1down_Loose','_CategoryReduction_JET_EffectiveNP_Mixed2__1up_Loose','_CategoryReduction_JET_EffectiveNP_Mixed3__1down_Loose','_CategoryReduction_JET_EffectiveNP_Mixed3__1up_Loose','_CategoryReduction_JET_EffectiveNP_Modelling1__1down_Loose','_CategoryReduction_JET_EffectiveNP_Modelling1__1up_Loose','_CategoryReduction_JET_EffectiveNP_Modelling2__1down_Loose','_CategoryReduction_JET_EffectiveNP_Modelling2__1up_Loose','_CategoryReduction_JET_EffectiveNP_Modelling3__1down_Loose','_CategoryReduction_JET_EffectiveNP_Modelling3__1up_Loose','_CategoryReduction_JET_EffectiveNP_Modelling4__1down_Loose','_CategoryReduction_JET_EffectiveNP_Modelling4__1up_Loose','_CategoryReduction_JET_EffectiveNP_Statistical1__1down_Loose','_CategoryReduction_JET_EffectiveNP_Statistical1__1up_Loose','_CategoryReduction_JET_EffectiveNP_Statistical2__1down_Loose','_CategoryReduction_JET_EffectiveNP_Statistical2__1up_Loose','_CategoryReduction_JET_EffectiveNP_Statistical3__1down_Loose','_CategoryReduction_JET_EffectiveNP_Statistical3__1up_Loose','_CategoryReduction_JET_EffectiveNP_Statistical4__1down_Loose','_CategoryReduction_JET_EffectiveNP_Statistical4__1up_Loose','_CategoryReduction_JET_EffectiveNP_Statistical5__1down_Loose','_CategoryReduction_JET_EffectiveNP_Statistical5__1up_Loose','_CategoryReduction_JET_EffectiveNP_Statistical6__1down_Loose','_CategoryReduction_JET_EffectiveNP_Statistical6__1up_Loose','_CategoryReduction_JET_EtaIntercalibration_Modelling__1down_Loose','_CategoryReduction_JET_EtaIntercalibration_Modelling__1up_Loose','_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down_Loose','_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up_Loose','_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down_Loose','_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up_Loose','_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down_Loose','_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up_Loose','_CategoryReduction_JET_EtaIntercalibration_TotalStat__1down_Loose','_CategoryReduction_JET_EtaIntercalibration_TotalStat__1up_Loose','_CategoryReduction_JET_Flavor_Composition__1down_Loose','_CategoryReduction_JET_Flavor_Composition__1up_Loose','_CategoryReduction_JET_Flavor_Response__1down_Loose','_CategoryReduction_JET_Flavor_Response__1up_Loose','_CategoryReduction_JET_JER_EffectiveNP_1__1up_Loose','_CategoryReduction_JET_JER_EffectiveNP_2__1up_Loose','_CategoryReduction_JET_JER_EffectiveNP_3__1up_Loose','_CategoryReduction_JET_JER_EffectiveNP_4__1up_Loose','_CategoryReduction_JET_JER_EffectiveNP_5__1up_Loose','_CategoryReduction_JET_JER_EffectiveNP_6__1up_Loose','_CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up_Loose','_CategoryReduction_JET_Pileup_OffsetMu__1down_Loose','_CategoryReduction_JET_Pileup_OffsetMu__1up_Loose','_CategoryReduction_JET_Pileup_OffsetNPV__1down_Loose','_CategoryReduction_JET_Pileup_OffsetNPV__1up_Loose','_CategoryReduction_JET_Pileup_PtTerm__1down_Loose','_CategoryReduction_JET_Pileup_PtTerm__1up_Loose','_CategoryReduction_JET_Pileup_RhoTopology__1down_Loose','_CategoryReduction_JET_Pileup_RhoTopology__1up_Loose','_CategoryReduction_JET_SingleParticle_HighPt__1down_Loose','_CategoryReduction_JET_SingleParticle_HighPt__1up_Loose']

            for syst in Syst :
	        print('Running on: '+syst+' systematic')
                process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarbb'+syst+'.root',tree_name,'','')
                process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarcc'+syst+'.root',tree_name,'','')
                process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarlight'+syst+'.root',tree_name,'','')

            if jet_collection == 'PFlow':
               process_actual_output_function(function_4j,function_5j,function_6j,input_path+'Wtocb'+syst+'.root',tree_name,'','')  

        if ttbar_sample == 'Alternative' :   
            #noShWMCPy8 
	    file_function_alt1_load_4j = TFile("outputnoShWMCPy8_funct_4j2b1loose.root")
	    ratio_histo_alt1_4j = file_function_alt1_load_4j.Get("ratio_histo")
	    file_function_alt1_load_5j = TFile("outputnoShWMCPy8_funct_5j2b1loose.root")
	    ratio_histo_alt1_5j = file_function_alt1_load_5j.Get("ratio_histo")
	    file_function_alt1_load_6j = TFile("outputnoShWMCPy8_funct_6j2b1loose.root")
	    ratio_histo_alt1_6j = file_function_alt1_load_6j.Get("ratio_histo")

            #PowH7
	    file_function_alt2_load_4j = TFile("outputPowH7_funct_4j2b1loose.root")
	    ratio_histo_alt2_4j = file_function_alt2_load_4j.Get("ratio_histo")
	    file_function_alt2_load_5j = TFile("outputPowH7_funct_5j2b1loose.root")
	    ratio_histo_alt2_5j = file_function_alt2_load_5j.Get("ratio_histo")
	    file_function_alt2_load_6j = TFile("outputPowH7_funct_6j2b1loose.root")
	    ratio_histo_alt2_6j = file_function_alt2_load_6j.Get("ratio_histo")

            # Ratio between alternative ttbar and nominal ttbar distributions for (data-nonttbar)/ttbar
            ratio_alt1_4j = ratio_histo_alt1_4j.Clone('ratio_alt1_4j')
            ratio_alt1_4j.Divide(ratio_histo_4j)
            ratio_alt1_5j = ratio_histo_alt1_5j.Clone('ratio_alt1_5j')
            ratio_alt1_5j.Divide(ratio_histo_5j)
            ratio_alt1_6j = ratio_histo_alt1_6j.Clone('ratio_alt1_6j')
            ratio_alt1_6j.Divide(ratio_histo_6j)

            ratio_alt2_4j = ratio_histo_alt2_4j.Clone('ratio_alt2_4j')
            ratio_alt2_4j.Divide(ratio_histo_4j)
            ratio_alt2_5j = ratio_histo_alt2_5j.Clone('ratio_alt2_5j')
            ratio_alt2_5j.Divide(ratio_histo_5j)
            ratio_alt2_6j = ratio_histo_alt2_6j.Clone('ratio_alt2_6j')
            ratio_alt2_6j.Divide(ratio_histo_6j)
            
            process_actual_output_alt_sample(ratio_alt1_4j,ratio_alt1_5j,ratio_alt1_6j,input_path+'ttbarbbnoShWMCPy8.root',tree_name,'noShWMCPy8') 
            process_actual_output_alt_sample(ratio_alt1_4j,ratio_alt1_5j,ratio_alt1_6j,input_path+'ttbarccnoShWMCPy8.root',tree_name,'noShWMCPy8') 
            process_actual_output_alt_sample(ratio_alt1_4j,ratio_alt1_5j,ratio_alt1_6j,input_path+'ttbarlightnoShWMCPy8.root',tree_name,'noShWMCPy8') 

            process_actual_output_alt_sample(ratio_alt2_4j,ratio_alt2_5j,ratio_alt2_6j,input_path+'ttbarbbPowH7.root',tree_name,'PowH7') 
            process_actual_output_alt_sample(ratio_alt2_4j,ratio_alt2_5j,ratio_alt2_6j,input_path+'ttbarccPowH7.root',tree_name,'PowH7') 
            process_actual_output_alt_sample(ratio_alt2_4j,ratio_alt2_5j,ratio_alt2_6j,input_path+'ttbarlightPowH7.root',tree_name,'PowH7') 

            # Store also nominal weight 
            process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarbbnoShWMCPy8.root',tree_name,'','')
            process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarccnoShWMCPy8.root',tree_name,'','')
            process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarlightnoShWMCPy8.root',tree_name,'','')

            process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarbbPowH7.root',tree_name,'','')
            process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarccPowH7.root',tree_name,'','')
            process_actual_output_function(function_4j,function_5j,function_6j,input_path+'ttbarlightPowH7.root',tree_name,'','')

            if jet_collection == 'PFlow' :
                process_actual_output_alt_sample(ratio_alt1_4j,ratio_alt1_5j,ratio_alt1_6j,input_path+'WtocbnoShWMCPy8.root',tree_name,'noShWMCPy8')
                process_actual_output_function(function_4j,function_5j,function_6j,input_path+'WtocbnoShWMCPy8.root',tree_name,'','')
                process_actual_output_alt_sample(ratio_alt2_4j,ratio_alt2_5j,ratio_alt2_6j,input_path+'WtocbPowH7.root',tree_name,'PowH7')
                process_actual_output_function(function_4j,function_5j,function_6j,input_path+'WtocbPowH7.root',tree_name,'','')

    #print("Test kde at 1500 MC/Data",(kernel_mc_load.Eval(1500),kernel_data_load.Eval(1500)))
        
#___________________________________________________________

if __name__=='__main__':
    main()
