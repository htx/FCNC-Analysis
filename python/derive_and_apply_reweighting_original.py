#!/bin/env python

from array import array
import os
import sys
import ROOT as R
import numpy as np
R.gROOT.SetBatch(1)

from ROOT import TKDE
from ROOT import TCanvas
from ROOT import TH1F
from ROOT import TF1
from ROOT import TFile

# Global config 
meff_n_bin = 12
meff_min_bin = 300
meff_max_bin = 1500

meff_cut = meff_min_bin

run_mode = 2 # 1=kernels, 2=fit function

def process_tree_for_kde(input_filename,tree_name,is_mc,debug=False):
    print('In process_tree_for_kde')
    input_data = TH1F("input_data","input_data",meff_n_bin,meff_min_bin,meff_max_bin)
    input_data.Sumw2()
    input_file = R.TFile.Open(input_filename)
    input_tree = input_file.Get(tree_name)
    meff_vec = R.std.vector('double')()
    weight_vec = R.std.vector('double')()
    for event in input_tree:
        if event.meff > meff_cut:
            if debug: 
                print('meff ',(event.meff))
            meff_vec.push_back(event.meff)
            if is_mc: 
                weight_vec.push_back(event.nomWeight_weight_btag*event.nomWeight_weight_jvt*event.nomWeight_weight_leptonSF*event.nomWeight_weight_mc*event.nomWeight_weight_norm*event.nomWeight_weight_pu)
                input_data.Fill(event.meff,event.nomWeight_weight_btag*event.nomWeight_weight_jvt*event.nomWeight_weight_leptonSF*event.nomWeight_weight_mc*event.nomWeight_weight_norm*event.nomWeight_weight_pu)
            else: 
                input_data.Fill(event.meff)
                weight_vec.push_back(1)
    input_file.Close()
    #kde = TKDE( len(meff_vec), meff_vec.data(), weight_vec.data(), 400, 1000, "KernelType:UserDefined;Iteration:Adaptive;Mirror:noMirror;Binning:ForcedBinning") 
    kde = TKDE( len(meff_vec), meff_vec.data(), weight_vec.data(),meff_min_bin,meff_max_bin)
    output_local = []
    output_local.append(kde)
    output_local.append(input_data)
    return output_local

def save_kde(kde,input_data,output_filename):
    print('In save_kde')
    central_value = kde.GetFunction()
    up_value = kde.GetUpperFunction()
    down_value = kde.GetLowerFunction()
    output_file = TFile(output_filename,'recreate')
    central_value.Write()
    up_value.Write()
    down_value.Write()
    input_data.Write()
    output_file.Close()

def derive_fit_function(input_mc,input_data,output_filename='output_funct.root'): 
    print('In derive_fit_function')
    output_file = TFile(output_filename,'recreate')
    input_mc_histo = input_mc.Get('input_data')
    input_data_histo = input_data.Get('input_data')
    # Norm hists to unity 
    input_mc_histo.Scale(1./input_mc_histo.Integral())
    input_data_histo.Scale(1./input_data_histo.Integral())
    # Ratio between unity normalised histograms, no normalisation correction
    ratio_histo = input_data_histo.Clone('ratio_histo')
    ratio_histo.Divide(input_mc_histo)
    f1 = TF1("f1","[0]+[1]*x+[2]*x*x",meff_min_bin,meff_max_bin)
    ratio_histo.Fit(f1)
    ratio_histo.Write()
    input_mc_histo.Write()
    input_data_histo.Write()
    f1.Write()
    output_file.Close()

def process_actual_output(kde_mc,kde_data,input_filename,tree_name): 
    print('In process_actual_output')
    input_file = R.TFile.Open(input_filename,'update')
    input_tree = input_file.Get(tree_name)
    rew_func  = array('d',[0])
    #rew_func  = 0
    branch_new_func = input_tree.Branch('branch_rew_func',rew_func, 'branch_rew_func/D')
    index = 0
    tot_entries = float(input_tree.GetEntries())
    for event in input_tree :
        #print('Processing event %i of %f', (index,tot_entries))
        if kde_mc.Eval(event.meff) != 0: 
            rew_func[0] = kde_data.Eval(event.meff) / kde_mc.Eval(event.meff)
        else : 
            rew_func[0] = 1.
        index = index +1
        branch_new_func.Fill()
        if  index > tot_entries:
            break
    input_file.Write()
    input_file.Close()

def process_actual_output_function(function,input_filename,tree_name, debug=False): 
    print('In process_actual_output_function')
    input_file = R.TFile.Open(input_filename,'update')
    input_tree = input_file.Get(tree_name)
    rew_func  = array('d',[0])
    branch_new_func = input_tree.Branch('branch_rew_func',rew_func, 'branch_rew_func/D')
    index = 0
    tot_entries = float(input_tree.GetEntries())
    for event in input_tree :
        if debug == True: 
            print('Processing event %i of %f', (index,tot_entries))
        rew_func[0] = function.Eval(event.meff)
        index = index +1
        branch_new_func.Fill()
        if  index > tot_entries:
            break
    input_file.Write()
    input_file.Close()


def main():
    if len(sys.argv) != 7 :
        print('Wrong arguments, example of command line:')
        print('python derive_and_apply_reweighting.py  /nfs/at3/scratch2/aivina/Fit_inputs_2020_DNN/Zjets22light.root /nfs/at3/scratch2/aivina/Fit_inputs_2020_DNN/Zjets22light.root Zjets22light.root True True True')
        return

    input_filename_mc = sys.argv[1]
    input_filename_data = sys.argv[2]
    input_filename_test = sys.argv[3]
    derive_kernel_mc = sys.argv[4]
    derive_kernel_data = sys.argv[5]
    evaluate_kernel = sys.argv[6]

    tree_name = 'tree'

    print('Options ')
    print('input_filename_mc ', input_filename_mc)
    print('input_filename_data ', input_filename_data)
    print('input_filename_test', input_filename_test)
    print('derive_kernel_mc', derive_kernel_mc)
    print('derive_kernel_data', derive_kernel_data)
    print('evaluate_kernel', evaluate_kernel)

    if derive_kernel_mc==True : 
        print('Deriving kernel for MC')
        mc_rew_info = process_tree_for_kde(input_filename_mc,tree_name,True)
        mc_kde = mc_rew_info[0]
        mc_input = mc_rew_info[1]
        save_kde(mc_kde,mc_input,'kernel_mc.root')
    
    if derive_kernel_data==True : 
        print('Deriving kernel for data')
        data_rew_info = process_tree_for_kde(input_filename_data,tree_name,False)
        data_kde = data_rew_info[0]
        data_input = data_rew_info[1]
        save_kde(data_kde,data_input,'kernel_data.root')
    
    file_mc_load = TFile("kernel_mc.root")
    kernel_mc_load = file_mc_load.Get("KDEFunc_")
    #kernel_mc_load.Draw()

    file_data_load = TFile("kernel_data.root")
    kernel_data_load = file_data_load.Get("KDEFunc_")
    #kernel_data_load.Draw()

    file_function_load = TFile("output_funct.root")
    function = file_function_load.Get("f1")
    
    if run_mode == 2:
        derive_fit_function(file_mc_load,file_data_load,output_filename='output_funct.root')

    if evaluate_kernel==True and run_mode == 1:
        process_actual_output(kernel_mc_load,kernel_data_load,input_filename_test,tree_name)
    
    if evaluate_kernel==True and run_mode == 2:
        process_actual_output_function(function,input_filename_test,tree_name)

    print("Test kde at 1500 MC/Data",(kernel_mc_load.Eval(1500),kernel_data_load.Eval(1500)))
        
#___________________________________________________________

if __name__=='__main__':
    main()
