#!/bin/python
import sys
import os
import string
import time, getpass
import socket
import datetime
from ROOT import *
from Analysis_Samples import *

sys.path.append( os.getenv("ROOTCOREBIN") + "python/IFAETopFramework/" )
from BatchTools import *
from Job import *
from Samples import *

##________________________________________________________
## OPTIONS
debug=True
nFilesSplit = 5000
nMerge=1
wgtSysSplit=False
##........................................................

##________________________________________________________
## Defines some useful variables
platform = socket.gethostname()
now = datetime.datetime.now().strftime("%Y_%m_%d_%H%M")
here = os.getcwd()
##........................................................

##________________________________________________________
## Defining the paths and the tarball
inputDir="/nfs/at3/scratch2/orlando/FCNC-Full-Run2/prod-june-2020/n/af2_folder/"
 
listFolder=here+"/Lists_Analysis_" + now
#following here how to get normalisaiton https://gitlab.cern.ch/atlasHTop/TTHbbAnalysis/blob/9c440f909ea0673ef3ecb5140bbffd4af844e459/TTHbbObjects/Root/TTHbbUtils.cxx L113
histogramName="cutflow_mc_Loose"
##........................................................

##________________________________________________________
## Creating usefull repositories
os.system("mkdir -p " + listFolder) #list files folder
##........................................................

##________________________________________________________
## Getting all samples and their associated weight/object systematics
Samples = []
Samples += GetTtbarSamples(hfSplitted=False,ttbarSystSamples=True, useHTSlices=False, useBFilter=True)
Samples += GetOtherSamples (includeSignals=True,includeSingletopSystSamples=True)
printGoodNews("--> All samples recovered")
##........................................................

##________________________________________________________
## Creating the config file
configFile = open("configFile_"+now+".dat","w")
configFile.write("#\n#\n")
configFile.write("# Path to files: " + inputDir + "\n")
configFile.write("# Date: " + now + "\n")
configFile.write("# Histogram used to normalize: "+ histogramName +"\n")
configFile.write("#\n#\n")

##________________________________________________________
## Looking into the XSe DB file
xsfile = open(os.getenv("ROOTCOREBIN") + "/data/FCNC-Analysis/XSection-MC15-13TeV.data","read")
print("Using cross section file "+os.getenv("ROOTCOREBIN") + "/data/FCNC-Analysis/XSection-MC15-13TeV.data")
searchlines = xsfile.readlines()
xsfile.close()

##________________________________________________________
## Loop over samples
printGoodNews("--> Performing the loop over samples")
for sample in Samples:

    SName = sample['name'] # sample name
    iDir = inputDir
    excluded = []

    os.chdir(iDir)
    joblist = getSampleJobs(sample,InputDir="",NFiles=nFilesSplit,UseList=False,
                            ListFolder=listFolder,exclusions=excluded,useDiffFilesForObjSyst=False)

    if( not joblist or len(joblist)!=1):
        printError("<!> The job for sample "+SName+" isn't standard: please check")
        continue

    files = joblist[0]['filelist'].split(",")
    nEventsWeighted = 0
    crossSection = -1
    dsid = SName

    for f in files:
        root_f = TFile(f,"read")
        tree = root_f.Get('sumWeights')
        tree.SetBranchStatus("*", 1)
        NEntries = tree.GetEntries()
        for ientry in range(NEntries):
            tree.GetEntry(ientry)
            nEventsWeighted+=tree.totalEventsWeighted

        #here you have access to the full string (directory, dataset name, file name)
        #accessing histograms for normalisation based on r-tags, one for MC campaign
        #mc16a, using 2015 folder only because of a bug in AnalysisTop which duplicates in the 2015 and 2016 folders the content of the sum of weights corresponging to 2015+2016
        #if "r9364" in f : 
        #    folders_with_cut_flows=["ejets_2015_DL1r"]
        #mc16d
        #elif "r10201" in f : 
        #    folders_with_cut_flows=["ejets_2017_DL1r"]
        #mc16e
        #elif "r10724" in f : 
        #    folders_with_cut_flows=["ejets_2018_DL1r"]
        #else : 
        #    printError("<!> The file "+f+" doesn't contain any of the supported r-tags")
        #    continue

        #if(root_f.IsZombie()):
        #    printError("<!> The file "+f+" is corrupted ... removes it from the list")
        #    continue

        #looping over all folders to get the the total number of weighted events
        #for folder in folders_with_cut_flows: 
            #getting the total number of weighted events 
        #    h_nEvents = root_f.Get(folder+"/"+histogramName).Clone()
        #    h_nEvents.SetDirectory(0)
        #    nEventsWeighted += h_nEvents.GetBinContent(1)

        #getting the cross section
        for line in searchlines:
            if( SName.replace(".","") in line ):
                splitted = line.split()
                dsidthisline = splitted[0]
                xsthisline = splitted[1]
                kfactorthisline = splitted[2]
                generator = splitted[3]
                crossSection = float(xsthisline)*float(kfactorthisline)
                break
    print(dsid)
    configFile.write(dsid+" "+`nEventsWeighted`+" "+`crossSection`+"\n")
    if crossSection < 0:
        printError("<!> The sample "+ SName +" is not in the cross-section file. Please check ! The cross-section is set to -1.")
        continue
    os.chdir(here)

configFile.close()

##________________________________________________________
## Removing list folder
os.system("rm -rf " + listFolder) #list files folder
##........................................................
