base_dir="/nfs/at3/scratch2/orlando/FCNC-offline"

#dirs_to_merge="FCNC-Analysis_output_fcnc_data-mc_round3_v0"
dirs_to_merge="FCNC-Analysis_output_fcnc_data-mc_round1_vhfsplitted0"

for directory in ${dirs_to_merge}
do 
    input_dir=${base_dir}"/"${directory}
    #example on how to relaunch jobs
    #python CheckOutputs.py input=${input_dir}/Scripts_Analysis_${directory}/JobCheck.chk  relaunch=TRUE queue=at3
    python CheckOutputs.py input=${input_dir}/Scripts_Analysis_${directory}/JobCheck.chk  
done 


for directory in ${dirs_to_merge}
do 
    input_dir=${base_dir}"/"${directory}
    output_dir=${input_dir}"_merged"
    python ../macros/macros_stats/PrepareInputFilesTRexFitter.py inputDir=${input_dir} outputDir=${output_dir}  
done

