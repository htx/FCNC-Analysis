## List of scripts available and how to use them. 

### Main scripts

Script for jobs submission on at3 batch

    Submit_Analysis.py
    
example of usage (have a look inside for more info about available options)

    python Submit_Analysis.py --INPUTDIR=/nfs/at3/scratch2/orlando/FCNC-Full-Run2/prod_24-Sept-2018/ --OUTPUTDIRSUFFIX=fcnc_data-mc_round3_v0

to remove samples change options in Submit_Analysis.py, for example here 

    https://gitlab.cern.ch/htx/FCNC-Analysis/blob/70047734edd0ed0c45dac907c79e2ee8c216bb88/python/Submit_Analysis.py#L27 

you can see what are the flags you can switch to change samples. If you need to process single top with Wt/s-chan/t-chan split and perhaps add the relevant systematics you should do the following, here 

    https://gitlab.cern.ch/htx/FCNC-Analysis/blob/70047734edd0ed0c45dac907c79e2ee8c216bb88/python/Submit_Analysis.py#L157

you should add at the end of the function arguments SingletopSystSamples=True and here 

    https://gitlab.cern.ch/htx/FCNC-Analysis/blob/70047734edd0ed0c45dac907c79e2ee8c216bb88/python/Analysis_Samples.py#L403

you should have something different from "simple".  Finally, in order to run alternative ttbar samples used for evaluation of systematic uncertainites you should change this line 

    https://gitlab.cern.ch/htx/FCNC-Analysis/blob/70047734edd0ed0c45dac907c79e2ee8c216bb88/python/Submit_Analysis.py#L154 

putting ttbarSystSamples = True.

Script to check that all files you expect to produce are actually produced 
       
    CheckOutputs.py

example of usage (have a look inside for more info about available options)

    base_dir="/nfs/at3/scratch2/orlando/FCNC-offline"
    dirs_to_merge="FCNC-Analysis_output_fcnc_data-mc_round3_v0"
    input_dir=${base_dir}"/"${directory}
    python CheckOutputs.py input=${input_dir}/Scripts_Analysis_${directory}/JobCheck.chk  


Script to merge root files according to some reasonable grouping 

    macros/macros_stats/PrepareInputFilesTRexFitter.py

example of usage (have a look inside for more info about available options)

    base_dir="/nfs/at3/scratch2/orlando/FCNC-offline"
    dirs_to_merge="FCNC-Analysis_output_fcnc_data-mc_round3_v0"
    input_dir=${base_dir}"/"${directory}
    output_dir=${input_dir}"_merged"    
    python ../macros/macros_stats/PrepareInputFilesTRexFitter.py inputDir=${input_dir} outputDir=${output_dir}  


**Important**: Typical chain involves running a production with Submit_Analysis.py, check that all files are there with CheckOutputs.py, merge inputs with PrepareInputFilesTRexFitter.py 

### Reweighting script

To setup:
$ lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"

To specify the path for the input samples, change line 245/248 in derive_and_apply_reweighting.py:
input_path = '/nfs/at3/scratch2/mdaneri/Reweighting/NewCode/Samples/'

To run:

python derive_and_apply_reweighting.py Run_mode Jet_Collection Systematics ttbar_sample_type

Options:
Run_mode = 1/2/3
Jet_Collection = EMTopo/PFlow
Systematics = False/True (while False only stores weight branches on nominal samples, otherwise they are stored in nominal+systematic samples) 
ttbar_sample_type = Nominal/Alternative (runs over nominal ttbar generator samples or over the other 2 alternative options, nominal needs to be run first and the 2 steps have to be run with the Alternative option)

The code runs in 3 consecutive steps. They need to be run in this order 1, 2 and 3.

Step 1: creates rootfiles containing the meff histograms for each region and sample type (data, ttbar, non ttbar)

Step 2: (data-non ttbar)/ttbar is calculated and fitted with a quadratic function, each result is stored in a different rootfile depending on the region

Step 3: meff weight values are taken from the function and stored in the different ttbar samples splitted by flavour content

### Original Reweighting script 

This script can be used for evaluating a reweighting function to be applied to MC ttbar. It runs in two setps 

1. Derive a non-parametric smooth function out of meff for data and MC, e.g. ttbar (using Kernel Density Estimation)
2. Derive and apply the reweighting to a given sample as ratio of these functions.

##### Fist step 

Needs total ttbar MC and data, your would run like 

    python derive_and_apply_reweighting.py  /nfs/at3/scratch2/aivina/Fit_inputs_2020_DNN/ttbar.root /nfs/at3/scratch2/aivina/Fit_inputs_2020_DNN/Data.root Data.root True True False

Note the combination of the flags at the end. 

##### Second step 

Can run after the fist step that creates the functions needed. It has to run on ttbar MC, you run like 

    python derive_and_apply_reweighting.py  /nfs/at3/scratch2/aivina/Fit_inputs_2020_DNN/ttbar.root /nfs/at3/scratch2/aivina/Fit_inputs_2020_DNN/Data.root /nfs/at3/scratch2/aivina/Fit_inputs_2020_DNN/ttbar.root False False Frue

##### Arguments 

1. MC ttbar (used to rerive the kernel)
2. Data (used to rerive the kernels)
3. File where you want to store the reweighting function, that is ttbar
4. Derive kernel for MC
5. Derive kernel for data
6. Propagate the reweighting to the file in argument 3

### Other utilities 

For most cases there are instructions placed in the hearder of the python files

Sample script to add new variables in pre-existing ntuples

    add_tlv_branch.py

Sum of weights for Monte Carlo samples as needed for normalising them 

    addup_weighted_events.py


Compare two root files 
    
    diff_ROOT_files.py


Print the branches available in a tree
    
    dump_branches.py

Merge regions used to define histograms, handy if you don't want to rerun a full production in order to redifine in a simple manner some regions 

    merge_regions.py

Produce text files per DSID which care used to normalise the Monte Carlo samples

    ProduceSampleDATfile.py

Prepare limits plots based on ntuple trex-fitter outputs (take updated version from Chen https://gitlab.cern.ch/htx/FCNC-Analysis/-/blob/master/macros/macros_nicola/DumpLimitPlot.py)
    
     DumpLimitPlot.py 

### More scripts are available in github

Apply TRF rescaling to fit inputs in a recursive manner 

    https://github.com/nicola-orlando/simple-scripts/blob/master/apply_rescaling_trf_samples.py 

Rebin fit inputs and plug in the overflow bin 

    https://github.com/nicola-orlando/simple-scripts/blob/master/rebin_recursive.py 

Simple plots of histograms in a root file 

    https://github.com/nicola-orlando/simple-scripts/blob/master/scan_output_file.C 
