#ifndef FCNC_TRFMANAGER_H
#define FCNC_TRFMANAGER_H

#include <vector>
#include <string>

//ITF
class AnalysisObject;
class WeightManager;
#include "IFAETopFramework/TRFManager.h"

//VA
class FCNC_OutputData;
class FCNC_NtupleData;
class FCNC_Options;
class FCNC_NtupleData;

//TRF
class TRFinterface;

class FCNC_TRFManager: public TRFManager {

public:


    // Standard C++ functions
    FCNC_TRFManager( FCNC_Options* opt, WeightManager* weightMngr, const FCNC_NtupleData* ntupData, FCNC_OutputData* outData );
    FCNC_TRFManager( const FCNC_TRFManager & );
    ~FCNC_TRFManager();

    // Specific functions
    void Init();
    using TRFManager::ComputeTRFWeights;
    bool ComputeTRFWeights();
    bool UpdateBTagging(const bool isIncl, const int req_nbjets);

protected:
    std::string GetShowerID(const std::string& sample);

private:
    FCNC_Options *m_opt;
    WeightManager* m_weightMngr;
    const FCNC_NtupleData* m_ntupData;
    FCNC_OutputData* m_outData;
    TRFinterface *m_trfint;
    std::string m_btag_calib_scheme;

};

#endif //FCNC_TRFMANAGER_H
