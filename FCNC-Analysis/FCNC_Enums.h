namespace FCNC_Enums {

    enum RejectCode {
        TRIGGER_REJECTED = 1,
        BADJET_REJECTED = 2,
        LEPTON_REJECTED = 3,
        TRIGGERMATCH_REJECTED = 4,
        JETS_REJECTED = 5,
        METMTW_REJECTED = 6,
        DPHI_REJECTED = 7,
        MEFF_REJECTED = 8,
        MAXMETONELEP_REJECTED = 9,
        METSIG_REJECTED = 10
    };

    enum ChannelType {
        UNDEFINED = 0,
        ELECTRON = 1,
        MUON = 2,
        FULLHAD = 3
    };

    enum TriggerType {
      TRIGELEC = 1,
      TRIGMUON = 2,
      TRIGMET = 3
    };
    enum DataPeriod { //Used as bit shifts
      DATA2015 = 0x1<<0,
      DATA2016 = 0x1<<1
    };

}
