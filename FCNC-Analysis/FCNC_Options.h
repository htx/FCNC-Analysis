#ifndef FCNC_OPTIONS_H
#define FCNC_OPTIONS_H

#include <string>
#include "IFAETopFramework/OptionsBase.h"

class FCNC_Options : public OptionsBase {

public:

  //slicing
  enum FilteringType {
    NOFILTER = 0,
    APPLYFILTER = 1
  };

  //
  // Standard C++ functions
  //
  FCNC_Options();
  FCNC_Options( const FCNC_Options & );
  ~FCNC_Options();

  //
  // Specific functions
  //
  virtual void PrintOptions();

  //
  // Getters
  //
  // bools
  inline bool UseLeptonsSF() const { return m_useLeptonsSF; }
  inline bool UseBtagSF() const { return m_useBtagSF; }
  inline bool RecomputeBtagSF() const { return m_recomputeBtagSF; }
  inline bool UsePileUpWeight() const { return m_usePileUpWeight; }
  inline bool DumpHistos() const { return m_dumpHistos; }
  inline bool DumpTree() const { return m_dumpTree; }
  inline bool DumpDLTree() const { return m_dumpDLTree; }
  inline bool DumpIsoTree() const { return m_dumpIsoTree; }
  inline bool DumpOverlapTree() const { return m_dumpOverlapTree; }
  inline bool DoTruthAnalysis() const { return m_doTruthAnalysis; }
  inline bool DoTRF() const { return m_doTRF; }
  inline bool DoPerBinTRF() const { return m_doPerBinTRF; }
  inline bool RecomputeTRF() const { return m_recomputeTRF; }
  inline bool ApplyMetMtwCuts() const { return m_applyMetMtwCuts; }
  inline bool InvertMetMtwCuts() const { return m_invertMetMtwCuts; }
  inline bool MultipleVariables() const { return m_multipleVariablesWithUncertainties; }
  inline bool DoCutFlow() const { return m_doCutFlow; }
  inline bool DoLowBRegions() const { return m_doLowBRegions; }
  inline bool DoSplitEMu() const { return m_doSplitEMu; }
  inline bool DoSumRegions() const { return m_doSumRegions; }
  inline bool ScaleTtbarHtSlices() const { return m_scaleTtbarHtSlices; }
  inline bool PFlowJetAlg() const { return m_pflowjetAlg; }
  // strings
  inline std::string BtagOP() const { return m_btagOP; }
  inline std::string BtagAlg() const { return m_btagAlg; }
  inline std::string LepIsoCut() const { return m_LepIsoCut; }
  inline std::string TRFCDIPath() const { return m_TRFCDIPath; }
  inline std::string SampleDat() const { return m_sampleDat; }
  // doubles
  inline double JetsPtCut() const { return m_jetsPtCut; }
  inline double JetsEtaCut() const { return m_jetsEtaCut; }
  inline double LepPtCut() const { return m_lepPtCut; }

  // ints
  inline int MaxTRFB() const { return m_maxb; }

  // enums
  inline FilteringType FilterType() const { return m_filterType; }
  inline FilteringType HFFilter() const {return m_hffilter; }
  /// check whether the options are consistent
  /**
  Raise std::logic_error if the options are not a vaild choice.
  */
  void checkConcistency() const;
protected:
  virtual bool IdentifyOption( const std::string &, const std::string & );

private:
  bool m_useLeptonsSF, m_useBtagSF, m_recomputeBtagSF, m_usePileUpWeight;
  bool m_dumpHistos, m_dumpTree, m_dumpIsoTree, m_dumpOverlapTree, m_dumpDLTree;
  bool m_doTruthAnalysis;
  bool m_doTRF, m_doPerBinTRF, m_recomputeTRF;
  bool m_applyMetMtwCuts;
  bool m_invertMetMtwCuts;
  bool m_applyTtbbCorrection;
  bool m_multipleVariablesWithUncertainties;
  bool m_useLeptonTrigger;
  bool m_doCutFlow;
  bool m_pflowjetAlg;

  bool m_doLowBRegions,
    m_doSplitEMu;
  bool m_doSumRegions;

  bool m_scaleTtbarHtSlices;
  int m_maxb;
  double m_jetsPtCut;
  double m_jetsEtaCut;
  double m_lepPtCut;

  std::string m_btagOP;
  std::string m_btagAlg;
  std::string m_LepIsoCut;
  std::string m_TRFCDIPath;
  std::string m_sampleDat;
  FilteringType m_filterType;
  FilteringType m_hffilter;
};

#endif //FCNC_OPTIONS_H
