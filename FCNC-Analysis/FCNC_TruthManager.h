#ifndef FCNC_TRUTHMANAGER_H
#define FCNC_TRUTHMANAGER_H

#include <vector>

class AnalysisObject;
class FCNC_Options;
class FCNC_NtupleData;

class FCNC_TruthManager {

public:


    // Standard C++ function
    FCNC_TruthManager( FCNC_Options* opt, const FCNC_NtupleData *ntup );
    FCNC_TruthManager( const FCNC_TruthManager & );
    ~FCNC_TruthManager();


    // Specific functions
    bool Initialize ();
    int PrintTruthContent() const;
    int FollowDecayChain( const int index, std::string &space ) const;
    double GetTopPt() const;

    // Getter
    inline std::vector< AnalysisObject* >* GetTruthParticleVector() { return m_truthParticle; };


private:
    FCNC_Options *m_opt;
    const FCNC_NtupleData *m_ntupData;
    std::vector< AnalysisObject* > *m_truthParticle;
    //
    std::vector< AnalysisObject* > *m_truthPartons;
    std::vector< AnalysisObject* > *m_truthMEParticles;
    bool m_dottbar;

};

#endif // FCNC_TRUTHMANAGER_H
