#include <iostream>

#include "IFAETopFramework/AnalysisObject.h"
#include "IFAETopFramework/TriggerInfo.h"

#include "FCNC-Analysis/FCNC_Options.h"
#include "FCNC-Analysis/FCNC_OutputData.h"


//______________________________________________________
//
FCNC_OutputData::FCNC_OutputData( FCNC_Options* opt ):
OutputData(),
m_opt(opt),
//Event variables
o_channel_type(0),o_period(0),o_run_number(0),o_pileup_mu(0),o_npv(0), o_meff(0), o_met(0), o_mtwl(0), o_ptwl(0), o_hthad(0), o_met_sig(0),
//Jet variables
o_jets_n(0),o_bjets_n(0),
o_jets(0),o_bjets(0),o_ljets(0),
o_bjets_lowb_3b(0),o_bjets_lowb_4b(0),
o_jets_btagworder_4j(0),
// discrete btagging weight                                                                                                                                                                               
o_btagw_discrete(0),o_btagw_discrete_bord(0),
//Lepton variables
o_el_n(0),o_mu_n(0),o_lep_n(0),o_lepForVeto_n(0),
o_el_loose_n(0),o_mu_loose_n(0),o_lep_loose_n(0),
o_el(0),o_mu(0),o_lep(0),o_selLep(0),
//MET
o_AO_met(0),
//Kinematic variables
o_dRmin_ejets(0), o_dRmin_mujets(0), o_dRmin_jetjet(0), o_dRmin_bjetbjet(0), o_dRmin_bjetbjet_lowb_3b(0), o_dRmin_bjetbjet_lowb_4b(0),
o_dR_TTL_bjets(0),o_dR_TTT_bjets(0), o_dR_TTLooser_bjets(0), 
o_mbb_mindR(0), o_mbb_mindR_lowb_3b(0), o_mbb_mindR_lowb_4b(0),
o_dPhi_lepmet(0), o_dPhi_jetmet(0), o_dPhi_jetmet5(0), o_dPhi_jetmet6(0), o_dPhi_jetmet7(0),o_dPhi_lepjet(0),o_dPhi_lepbjet(0),
o_dRmin_ebjets(0), o_dRmin_mubjets(0), o_mTbmin(0), o_mTbmin_lowb_3b(0), o_mTbmin_lowb_4b(0),
o_jets40_n(0), o_centrality(0), o_mbb_leading_bjets(0), o_mbb_softest_bjets(0), o_J_lepton_invariant_mass(0), o_J_leadingb_invariant_mass(0), o_J_J_invariant_mass(0),
o_dRaverage_bjetbjet(0),o_dRaverage_jetjet(0),
o_mbb_maxdR(0), o_dPhibb_leading_bjets(0), o_dPhibb_mindR(0), o_dPhibb_maxdR(0), o_dEtabb_mindR(0), o_dEtabb_maxdR(0), o_dEtabb_leading_bjets(0), o_mjj_leading_jets(0), 
o_mjj_mindR(0), o_mjj_maxdR(0), o_dPhijj_leading_jets(0), o_dPhijj_mindR(0), o_dPhijj_maxdR(0), o_dEtajj_leading_jets(0), o_dEtajj_mindR(0), o_dEtajj_maxdR(0),o_pseudo_mass(0),
o_mjb_mindR(0), o_mjb_maxdR(0), o_mjb_leading_bjet(0),
//Event variable for selection
o_rejectEvent(0),
//Truth variables
o_truth_dR_Wb(0), o_truth_top_pt(0),o_truth_ht_filter(0),o_truth_met_filter(0),
//TRF-weights storage
o_TRFweight_in(0),o_TRFweight_ex(0),o_TRFPerm_in(0),o_TRFPerm_ex(0),o_TRFbins_in(0),o_TRFbins_ex(0),
o_LTag_BreakUp_TRF_in(0), o_LTag_BreakDown_TRF_in(0), o_LTag_BreakUp_TRF_ex(0), o_LTag_BreakDown_TRF_ex(0),//light-tagging
o_CTag_BreakUp_TRF_in(0), o_CTag_BreakDown_TRF_in(0), o_CTag_BreakUp_TRF_ex(0), o_CTag_BreakDown_TRF_ex(0),//c-tagging
o_BTag_BreakUp_TRF_in(0), o_BTag_BreakDown_TRF_in(0), o_BTag_BreakUp_TRF_ex(0), o_BTag_BreakDown_TRF_ex(0),//b-tagging
o_BTagExtrapUp_TRF_in(0), o_BTagExtrapDown_TRF_in(0), o_BTagExtrapUp_TRF_ex(0), o_BTagExtrapDown_TRF_ex(0),//extrapolation
o_BTagExtrapFromCharmUp_TRF_in(0), o_BTagExtrapFromCharmDown_TRF_in(0), o_BTagExtrapFromCharmUp_TRF_ex(0), o_BTagExtrapFromCharmDown_TRF_ex(0),//extrapolation from charm 
o_is_ttbar(false), o_TopHeavyFlavorFilterFlag(0), 
// ISR and FSR weight
o_nom_weight_ISR_UP(0), o_nom_weight_ISR_DOWN(0), o_nom_weight_FSR_UP(0), o_nom_weight_FSR_DOWN(0),
o_nom_weight_index_1(0), o_nom_weight_index_2(0), o_nom_weight_index_3(0), o_nom_weight_index_4(0), o_nom_weight_index_5(0), o_nom_weight_index_6(0), o_nom_weight_index_7(0), o_nom_weight_index_8(0), o_nom_weight_index_193(0), o_nom_weight_index_194(0), o_nom_weight_index_198(0), o_nom_weight_index_199(0) 
{

  o_trigger_list.clear();

  o_region = new std::vector < int >;

  o_jets    = new AOVector();
  o_bjets   = new AOVector();
  if(m_opt->DoLowBRegions()){
    o_bjets_lowb_3b = new AOVector();
    o_bjets_lowb_4b = new AOVector();
  }
  o_fjets   = new AOVector();
  o_ljets   = new AOVector();
  o_jets_btagworder_4j = new AOVector(); 

  o_el = new AOVector();
  o_mu = new AOVector();
  o_lep = new AOVector();

  o_TRFweight_in      = new std::vector < double >;
  o_TRFweight_ex      = new std::vector < double >;
  o_TRFPerm_in        = new std::vector < std::vector< bool > >;
  o_TRFPerm_ex        = new std::vector < std::vector< bool > >;
  o_TRFbins_in         = new std::vector < std::vector< int > >;
  o_TRFbins_ex         = new std::vector < std::vector< int > >;

  o_BTag_BreakUp_TRF_in   = new std::vector < std::vector < double > >;
  o_BTag_BreakDown_TRF_in = new std::vector < std::vector < double > >;
  o_CTag_BreakUp_TRF_in   = new std::vector < std::vector < double > >;
  o_CTag_BreakDown_TRF_in = new std::vector < std::vector < double > >;
  o_LTag_BreakUp_TRF_in   = new std::vector < std::vector < double > >;
  o_LTag_BreakDown_TRF_in = new std::vector < std::vector < double > >;
  o_BTag_BreakUp_TRF_ex   = new std::vector < std::vector < double > >;
  o_BTag_BreakDown_TRF_ex = new std::vector < std::vector < double > >;
  o_CTag_BreakUp_TRF_ex   = new std::vector < std::vector < double > >;
  o_CTag_BreakDown_TRF_ex = new std::vector < std::vector < double > >;
  o_LTag_BreakUp_TRF_ex   = new std::vector < std::vector < double > >;
  o_LTag_BreakDown_TRF_ex = new std::vector < std::vector < double > >;
  o_BTagExtrapUp_TRF_ex   = new std::vector < std::vector < double > >;
  o_BTagExtrapDown_TRF_ex = new std::vector < std::vector < double > >;
  o_BTagExtrapUp_TRF_in   = new std::vector < std::vector < double > >;
  o_BTagExtrapDown_TRF_in = new std::vector < std::vector < double > >;
  o_BTagExtrapFromCharmUp_TRF_in    = new std::vector < std::vector < double > >;
  o_BTagExtrapFromCharmDown_TRF_in  = new std::vector < std::vector < double > >;
  o_BTagExtrapFromCharmUp_TRF_ex    = new std::vector < std::vector < double > >;
  o_BTagExtrapFromCharmDown_TRF_ex  = new std::vector < std::vector < double > >;
}

//______________________________________________________
//
FCNC_OutputData::FCNC_OutputData( const FCNC_OutputData & q ):
OutputData(q)
{
  std::cout << "<!> WARNING: You're using an empty copy-constructor (FCNC_OutputData) ... Please check !!" << std::endl;
}

//______________________________________________________
//
FCNC_OutputData::~FCNC_OutputData()
{
  ClearOutputData();

  for(TriggerInfo* trig : o_trigger_list){ delete trig; }
  o_trigger_list.clear();

  delete o_jets;
  delete o_bjets;
  delete o_bjets_lowb_3b;
  delete o_bjets_lowb_4b;
  delete o_fjets;
  delete o_ljets;
  delete o_jets_btagworder_4j;
  delete o_el;
  delete o_mu;
  delete o_lep;
}

//______________________________________________________
//
void FCNC_OutputData::ClearOutputData()
{
  //
  // The ClearOutputData function has to be call at each event in
  // order to restore the original value of all variables, keeping
  // constant the address.
  //
  OutputData::ClearOutputData();
  for(TriggerInfo* trig : o_trigger_list){
    trig->SetPass(false);
  }
  o_TopHeavyFlavorFilterFlag = -1;
  //
  // Flat variables
  //
  o_dRmin_ejets = 99.;
  o_dRmin_mujets = 99.;
  o_dRmin_jetjet = 99.;
  o_dRmin_bjetbjet = 99.;
  o_dRmin_bjetbjet_lowb_3b = 99.;
  o_dRmin_bjetbjet_lowb_4b = 99.;
  o_mbb_mindR = -1.;
  o_mbb_mindR_lowb_3b = -1.;
  o_mbb_mindR_lowb_4b = -1.;
  o_dPhi_lepmet = 99.;
  o_dPhi_jetmet = 99.;
  o_dPhi_jetmet5 = 99.;
  o_dPhi_jetmet6 = 99.;
  o_dPhi_jetmet7 = 99.;
  o_dPhi_lepjet = 99.;
  o_dPhi_lepbjet = 99.;
  o_dRmin_ebjets = 99.;
  o_dRmin_mubjets = 99.;
  o_mTbmin = 99.;
  o_mTbmin_lowb_3b = 99.;
  o_mTbmin_lowb_4b = 99.;

  o_mu_n = 0;
  o_el_n = 0;
  o_lep_n = 0;
  o_lepForVeto_n = 0;
  o_mu_loose_n = 0;
  o_el_loose_n = 0;
  o_lep_loose_n = 0;
  o_jets_n = 0;
  o_bjets_n = 0;
  o_fjets_n = 0;
  o_channel_type = 0;
  o_period = 0;
  o_run_number = 0;
  o_pileup_mu = 0;
  o_npv = 0;
  o_meff = 0;
  o_met = 0;
  o_mtwl = 0;
  o_ptwl = 0;
  o_hthad = 0;
  o_met_sig = 0;

  o_jets40_n = -100.;
  o_centrality = -100.;
  o_mbb_leading_bjets = -100.;
  o_mbb_softest_bjets = -100.;
  o_J_lepton_invariant_mass = -100.;
  o_J_leadingb_invariant_mass = -100.;
  o_J_J_invariant_mass = -100.;
  o_dRaverage_bjetbjet = -100; 
  o_dRaverage_jetjet = -100;

  o_mbb_maxdR = -1.;
  o_dPhibb_leading_bjets=99.;
  o_dPhibb_mindR=99.;
  o_dPhibb_maxdR=99.;
  o_dEtabb_leading_bjets=99.;
  o_dEtabb_mindR=99.;
  o_dEtabb_maxdR=99.;

  o_mjb_mindR=-1.; 
  o_mjb_maxdR=-1.; 
  o_mjb_leading_bjet=-1.;

  o_mjj_leading_jets=-1.;
  o_mjj_mindR=-1.;
  o_mjj_maxdR=-1.;

  o_dPhijj_leading_jets=99.;
  o_dPhijj_mindR=99.;
  o_dPhijj_maxdR=99.;

  o_dEtajj_leading_jets=99.;
  o_dEtajj_mindR=99.;
  o_dEtajj_maxdR=99.;

  o_pseudo_mass=-100;

  //
  // Selected lepton
  //
  o_selLep = 0;

  //
  // Vectors
  //
  //delete discrete btagging weight
  o_btagw_discrete.clear();
  o_btagw_discrete_bord.clear();

  //delete electrons
  for ( const AnalysisObject* ele : *(o_el) ) {
    delete ele;
  }
  o_el -> clear();

  //delete muons
  for ( const AnalysisObject* muon : *(o_mu) ) {
    delete muon;
  }
  o_mu -> clear();
  o_lep -> clear();

  //delete jets
  for ( const AnalysisObject* jet : *(o_jets) ) {
    delete jet;
  }
  o_jets -> clear();
  o_bjets -> clear();
  if(o_bjets_lowb_3b) o_bjets_lowb_3b -> clear();
  if(o_bjets_lowb_4b) o_bjets_lowb_4b -> clear();
  o_ljets->clear();

  o_fjets -> clear();
  o_jets_btagworder_4j -> clear();

  //delete met
  delete o_AO_met;
  o_AO_met = nullptr;

  o_region -> clear();

  //
  // Event variable for selection
  //
  o_rejectEvent = 0;

  //
  // Truth variables
  //
  o_truth_dR_Wb = -1;
  o_truth_top_pt = 0;
  o_truth_ht_filter = 0.;
  o_truth_met_filter = 0.;

}
