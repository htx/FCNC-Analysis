//Adding weights from ntuples according to IFAETopFramework functionalities 
//AddAndInitWeight( name, title, isNominal, isInput, branchName, replace, input_varType, atoi(range.c_str()) );

#include "FCNC-Analysis/FCNC_WeightManager.h"

#include "IFAETopFramework/SampleInfo.h"
#include "IFAETopFramework/AnalysisObject.h"
#include "IFAETopFramework/Selection.h"
#include "IFAETopFramework/AnalysisUtils.h"

#include "FCNC-Analysis/FCNC_Options.h"
#include "FCNC-Analysis/FCNC_NtupleData.h"
#include "FCNC-Analysis/FCNC_OutputData.h"
#include "FCNC-Analysis/FCNC_Enums.h"
#include "FCNC-Analysis/FCNC_VariableComputer.h"

#include <iostream>
#include <stdexcept> // invalid_argument
#include <string>
#include <map>

using std::string;


//______________________________________________________________________________
//
FCNC_WeightManager::FCNC_WeightManager( FCNC_Options *opt, const FCNC_NtupleData* ntupleData,
                                      FCNC_OutputData* outputData ):
WeightManager(opt, ntupleData, outputData),
m_fcnc_opt(opt),
m_fcnc_ntupData(ntupleData),
m_fcnc_outData(outputData),
m_sampleInfo(0),
m_varComputer(0)
{
}

//______________________________________________________________________________
//
FCNC_WeightManager::FCNC_WeightManager( const FCNC_WeightManager &q ) : WeightManager(q) {
  m_fcnc_opt           = q.m_fcnc_opt;
  m_fcnc_ntupData      = q.m_fcnc_ntupData;
  m_fcnc_outData       = q.m_fcnc_outData;
  m_sampleInfo        = q.m_sampleInfo;
  m_varComputer       = q.m_varComputer;
}

//______________________________________________________________________________
//
FCNC_WeightManager::~FCNC_WeightManager(){
    if(m_sampleInfo) delete m_sampleInfo;
    if(m_varComputer) delete m_varComputer;
}

//______________________________________________________________________________
//
//void FCNC_WeightManager::Init( std::map < int, Selection* >* selection_tree ){
void FCNC_WeightManager::Init(){

  // Cross-section weight
  if ( !(m_fcnc_opt -> IsData() || (m_opt -> StrSampleName().find("QCD") != std::string::npos)) ) {
    std::string path = "";
    path += std::getenv("ROOTCOREBIN") + std::string("/data/FCNC-Analysis/") + m_fcnc_opt -> SampleDat();
    string sampleId = m_fcnc_opt -> StrSampleID();
    m_sampleInfo = new SampleInfo( sampleId, path );
    if(!m_sampleInfo->Ready()){
      throw std::invalid_argument(string(__FILE__)+"\n"+
				  " Could not identify sample '"+sampleId+"'"
				  " in the provided config file '"+path+"'. \n"
				  " Please check !"
				  " Normalisation will be crap !");
    }
  }
  m_varComputer = new FCNC_VariableComputer(m_fcnc_opt);

}

//______________________________________________________________________________
//
bool FCNC_WeightManager::AddFCNCNominalWeights(){
  
  //AddAndInitWeight("mc_generator_weights", "" , true, true, "mc_generator_weights");
  AddAndInitWeight("weight_mc", "", true, true, "weight_mc","","F");
  AddAndInitWeight("weight_jvt", "", true, true, "weight_jvt","","F");
  AddAndInitWeight("weight_norm", "", true, false);

  if(m_fcnc_opt->UsePileUpWeight()){
    AddAndInitWeight("weight_pu", "", true, true, "weight_pileup","","F");
  }
  
  if(m_fcnc_opt->UseLeptonsSF()){
    AddAndInitWeight("weight_leptonSF", "", true, true, "weight_leptonSF","","F");;
  }

  if((m_fcnc_opt -> DoTRF() && m_fcnc_opt -> RecomputeTRF()) || m_fcnc_opt -> RecomputeBtagSF()){
    //currently broken--issue 11
    //AddAndInitWeight("weight_btag", "", true, false);
  } else {
    //get btagging scale factor from the ntuple
    if(m_fcnc_opt->BtagAlg()=="MV2c10") AddAndInitWeight("weight_btag","",true,true,"weight_bTagSF_MV2c10_Continuous","","F");
    if(m_fcnc_opt->BtagAlg()=="DL1r") AddAndInitWeight("weight_btag","",true,true,"weight_bTagSF_DL1r_Continuous","","F");
  }

  //if( m_fcnc_outData -> o_is_ttbar ){
    //ttbb correction weight
    //if(m_fcnc_opt->ApplyTtbbCorrection() && m_fcnc_opt->SampleName()==SampleName::TTBARBB){
    //commenting this out until we can propagate it ..
    //if(m_fcnc_opt->SampleName()==SampleName::TTBARBB){
    //  AddAndInitWeight("weight_ttbb", "", true, true, "weight_ttbb_ttbb_Nominal_weight");
    //}
    //ttbar fraction reweighting
    //if(m_fcnc_opt->ReweightTtbarFractions()){
    //AddAndInitWeight("weight_ttbar_fractions_rw", "", true, false);
  //}//ttbar
  
  return true;
}

//______________________________________________________________________________
//
bool FCNC_WeightManager::AddFCNCSystematicWeights(){

  //JVT systematics
  AddAndInitWeight("weight_jvt_UP"  , "", false, true, "weight_jvt_UP", "weight_jvt", "F");
  AddAndInitWeight("weight_jvt_DOWN", "", false, true, "weight_jvt_DOWN", "weight_jvt", "F");

  //PU systematics
  if(m_fcnc_opt->UsePileUpWeight()){
    AddAndInitWeight("weight_pu_UP", "", false, true, "weight_pileup_UP", "weight_pu", "F");
    AddAndInitWeight("weight_pu_DOWN", "", false, true, "weight_pileup_DOWN", "weight_pu", "F");
  }

  //Lepton SF systematics
  //example weight_indiv_SF_EL_ID_DOWN
  if(m_fcnc_opt->UseLeptonsSF()){
    std::vector<std::string> el_sys_comp = {"Reco", "ID", "Isol"};
    for(const std::string& el_sys : el_sys_comp){      
      AddAndInitWeight("weight_leptonSF_EL_SF_"+el_sys+"_UP", "", false, true, "weight_leptonSF_EL_SF_"+el_sys+"_UP", "weight_leptonSF", "F");
      AddAndInitWeight("weight_leptonSF_EL_SF_"+el_sys+"_DOWN", "", false, true, "weight_leptonSF_EL_SF_"+el_sys+"_DOWN","weight_leptonSF", "F");
    }
    
    //example weight_indiv_SF_MU_ID_STAT_DOWN
    //{"BADMUON", {"STAT", "SYST"}} is removed from the AnalysisTop output, to be checked
     std::map<std::string, std::vector<std::string> > mu_sys_comp = {
      {"ID", {"STAT", "SYST", "STAT_LOWPT", "SYST_LOWPT"}},
      {"Isol",{"STAT", "SYST"}},
      {"TTVA", {"STAT", "SYST"}}
    };
    for(std::pair<std::string, std::vector<std::string> > mu_sys_pair : mu_sys_comp){
      for(const std::string& mu_sys : mu_sys_pair.second){
        AddAndInitWeight("weight_leptonSF_MU_SF_"+mu_sys_pair.first+"_"+mu_sys+"_UP","",false, true,"weight_leptonSF_MU_SF_"+mu_sys_pair.first+"_"+mu_sys+"_UP", "weight_leptonSF", "F"); 
	AddAndInitWeight("weight_leptonSF_MU_SF_"+mu_sys_pair.first+"_"+mu_sys+"_DOWN","",false, true,"weight_leptonSF_MU_SF_"+mu_sys_pair.first+"_"+mu_sys+"_DOWN", "weight_leptonSF", "F");
      }
    }

    //trigger related
    std::vector<std::string> el_trig_sys_comp = {"Trigger"};
    for(const std::string& el_trig_sys : el_trig_sys_comp){
      AddAndInitWeight("weight_leptonSF_EL_SF_"+el_trig_sys+"_UP", "", false, true, "weight_leptonSF_EL_SF_"+el_trig_sys+"_UP",  "weight_leptonSF", "F");
      AddAndInitWeight("weight_leptonSF_EL_SF_"+el_trig_sys+"_DOWN", "", false, true, "weight_leptonSF_EL_SF_"+el_trig_sys+"_DOWN", "weight_leptonSF", "F");
    }
    std::vector<std::string> mu_trig_sys_comp = {"Trigger_STAT", "Trigger_SYST"};
    for(const std::string& mu_trig_sys : mu_trig_sys_comp){
      AddAndInitWeight("weight_leptonSF_MU_SF_"+mu_trig_sys+"_UP", "", false, true, "weight_leptonSF_MU_SF_"+mu_trig_sys+"_UP",  "weight_leptonSF", "F"); 
      AddAndInitWeight("weight_leptonSF_MU_SF_"+mu_trig_sys+"_DOWN", "", false, true, "weight_leptonSF_MU_SF_"+mu_trig_sys+"_DOWN",  "weight_leptonSF", "F");
    }

  }//lepton SF

  //Btag systematics
  //For btaggin EV we use more advanced IFAETopFramework features, see WeightManager 
  //WeightObject* WeightManager::AddAndInitWeight( const std::string &name, const std::string &title, bool isNominal,
  //bool isInput, const std::string& branchName,
  //const std::string& affected_component,
  //const std::string& componentType, int vec_ind ) {

  std::string btag_name = "";
  std::string btag_vartype = "";
  bool btag_isinput = true;
  if((m_fcnc_opt -> DoTRF() && m_fcnc_opt -> RecomputeTRF()) || m_fcnc_opt -> RecomputeBtagSF()){
    //currently broken--issue 11    
    btag_name = "weight_btag";
    btag_isinput = false;
  }
  else{
    btag_name = "weight_bTag";
    if(m_fcnc_opt->BtagAlg()=="MV2c10") btag_name = "weight_bTagSF_MV2c10_Continuous";
    if(m_fcnc_opt->BtagAlg()=="DL1r") btag_name = "weight_bTagSF_DL1r_Continuous";
  }

  for(int i = 0; i <=44; i++ ){
    AddAndInitWeight(Form("%s_eigenvars_B_%i_up",btag_name.c_str(),i  ), "", false, 
		     btag_isinput, Form("%s_eigenvars_B_up",btag_name.c_str())  , "weight_btag", "PVF",i);

    AddAndInitWeight(Form("%s_eigenvars_B_%i_down",btag_name.c_str(),i  ), "", false, 
		     btag_isinput, Form("%s_eigenvars_B_down",btag_name.c_str())  , "weight_btag", "PVF",i);
  }

  for(int i = 0; i <=14; i++ ){
    AddAndInitWeight(Form("%s_eigenvars_C_%i_up",btag_name.c_str(),i  ), "", false, 
		     btag_isinput, Form("%s_eigenvars_C_up",btag_name.c_str())  , "weight_btag", "PVF",i);

    AddAndInitWeight(Form("%s_eigenvars_C_%i_down",btag_name.c_str(),i  ), "", false, 
		     btag_isinput, Form("%s_eigenvars_C_down",btag_name.c_str())  , "weight_btag", "PVF",i);
  }

  for(int i = 0; i <=19; i++ ){
    AddAndInitWeight(Form("%s_eigenvars_Light_%i_up",btag_name.c_str(),i  ), "", false, 
		     btag_isinput, Form("%s_eigenvars_Light_up",btag_name.c_str())  , "weight_btag", "PVF",i);

    AddAndInitWeight(Form("%s_eigenvars_Light_%i_down",btag_name.c_str(),i  ), "", false, 
		     btag_isinput, Form("%s_eigenvars_Light_down",btag_name.c_str())  , "weight_btag", "PVF",i);
  }

  //ttbar systematics
  if( m_fcnc_outData -> o_is_ttbar ){

    //ttbb uncertainties
    //commenting this out for the time being, might be a good idea to plug that in again
    /*
    if(m_fcnc_opt->ApplyTtbbCorrection() && m_fcnc_opt->SampleName()==SampleName::TTBARBB){
    std::vector<std::string> ttbb_sys_comp = {"CSS_KIN", "MSTW", "NNPDF", "Q_CMMPS", "glosoft", "defaultX05", "defaultX2", "MPIup", "MPIdown", "MPIfactor", "aMcAtNloHpp", "aMcAtNloPy8"};
      for(const std::string& ttbb_sys : ttbb_sys_comp){
        AddAndInitWeight("weight_ttbb_"+ttbb_sys, "", false, true, "weight_ttbb_ttbb_"+ttbb_sys+"_weight", "weight_ttbb");
      }
    }//ttbb correction
    */

  }//ttbar samples

  return true;
}

//______________________________________________________________________________
//
bool FCNC_WeightManager::SetLeptonSFWeights( const bool apply_trigger_weights ){
  bool apply_weights = true;
  if ( m_fcnc_outData-> o_channel_type != FCNC_Enums::ELECTRON && m_fcnc_outData-> o_channel_type != FCNC_Enums::MUON ){
    apply_weights = false;
  }

  //Setting the nominal lepton weights to 1
  if(!apply_weights){
    for( auto& weight : *m_nomMap ){
      if( weight.first.find("weight_elec") != std::string::npos ||
          weight.first.find("weight_muon") != std::string::npos
      ){
        UpdateNominalComponent(weight.first, 1.);
      }
    }
  }
  //Setting the nominal trigger weight to 1 in case the event is trigger by met trigger
  else if(!apply_trigger_weights){
    for( auto& weight : *m_nomMap ){
      if( weight.first.find("weight_elec_trigger") != std::string::npos ||
          weight.first.find("weight_muon_trigger") != std::string::npos
      ){
        UpdateNominalComponent(weight.first, 1.);
      }
    }
  }

  //Setting the systematics lepton weights to 1
  if(!apply_weights){
    for( auto& weight : *m_systMap ){
      if( weight.first.find("weight_elec") != std::string::npos ||
          weight.first.find("weight_muon") != std::string::npos
      ){
        UpdateSystematicComponent(weight.first, 1.);
      }
    }
  }
  //Setting the nominal trigger weight to 1 in case the event is trigger by met trigger
  else if(!apply_trigger_weights){
    for( auto& weight : *m_systMap ){
      if( weight.first.find("weight_elec_trigger") != std::string::npos ||
          weight.first.find("weight_muon_trigger") != std::string::npos
      ){
        UpdateSystematicComponent(weight.first, 1.);
      }
    }
  }
  return true;
}

//______________________________________________________________________________
//
bool FCNC_WeightManager::SetTtbarHtSliceScale(){

  SetNominalComponent("weight_ttbar_htslice", 1.);
  if( m_fcnc_ntupData -> d_runNumber == 407009 ){ SetNominalComponent("weight_ttbar_htslice", 1.02849); }
  else if( m_fcnc_ntupData -> d_runNumber == 407010 ){ SetNominalComponent("weight_ttbar_htslice", 1.02637); }
  else if( m_fcnc_ntupData -> d_runNumber == 407011 ){ SetNominalComponent("weight_ttbar_htslice", 0.872646); }
  return true;
}

//______________________________________________________________________________
//
bool FCNC_WeightManager::SetCrossSectionWeight(){
  if(!m_sampleInfo){
    std::cerr << "<!> Error in FCNC_WeightManager::SetCrossSectionWeight(): m_sampleInfo is null ... Please check !" << std::endl;
    abort();
  }
  if( m_fcnc_opt -> IsData() || m_opt -> StrSampleName().find("QCD") != std::string::npos ){
    return false;
  }
  SetNominalComponent( "weight_norm", m_sampleInfo -> NormFactor() );
  return true;
}
