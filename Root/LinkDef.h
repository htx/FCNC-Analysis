#ifdef __CINT__

#include "IFAETopFramework/AnalysisObject.h"

#include "FCNC-Analysis/FCNC_Analysis.h"
#include "FCNC-Analysis/FCNC_AnalysisTools.h"
#include "FCNC-Analysis/FCNC_Enums.h"
#include "FCNC-Analysis/FCNC_NtupleData.h"
#include "FCNC-Analysis/FCNC_NtupleReader.h"
#include "FCNC-Analysis/FCNC_Options.h"
#include "FCNC-Analysis/FCNC_OutputData.h"
#include "FCNC-Analysis/FCNC_Selector.h"
#include "FCNC-Analysis/FCNC_TRFManager.h"
#include "FCNC-Analysis/FCNC_TruthManager.h"
#include "FCNC-Analysis/FCNC_VariableComputer.h"
#include "FCNC-Analysis/FCNC_WeightManager.h"

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class FCNC_Analysis+;
#pragma link C++ class FCNC_AnalysisTools+;

#pragma link C++ class FCNC_NtupleData+;
#pragma link C++ class FCNC_NtupleReader+;
#pragma link C++ class FCNC_Options+;
#pragma link C++ class FCNC_OutputData+;
#pragma link C++ class FCNC_Selector;
#pragma link C++ class FCNC_TRFManager;
#pragma link C++ class FCNC_TruthManager;
#pragma link C++ class FCNC_VariableComputer;
#pragma link C++ class FCNC_WeightManager;

#endif
